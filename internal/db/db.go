package db

import (
	"errors"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/jinzhu/gorm"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

const tcpMax int = 65535

type Config struct {
	User     string `json:"user,omitempty"`
	Password string `json:"password,omitempty"`
	Database string `json:"database,omitempty"`
	Host     string `json:"host,omitempty"`
	Port     int    `json:"port,omitempty"`
	Insecure bool   `json:"insecure,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.User, validation.Required),
		validation.Field(&c.Password, validation.Required),
		validation.Field(&c.Database, validation.Required),
		validation.Field(&c.Host, validation.Required, is.Host),
		validation.Field(&c.Port, validation.Min(1), validation.Max(tcpMax)),
	)
}

type SQLDatastore struct {
	DB  *gorm.DB
	url string
}

func New(config *Config) *SQLDatastore {
	port := 5432
	if config.Port != 0 {
		port = config.Port
	}

	connectionString := fmt.Sprintf(
		"host=%s user=%s dbname=%s password=%s port=%d",
		config.Host,
		config.User,
		config.Database,
		config.Password,
		port,
	)

	if config.Insecure {
		connectionString += " sslmode=disable"
	}

	return &SQLDatastore{
		url: connectionString,
	}
}

func (db *SQLDatastore) Name() string {
	return "Database"
}

func (db *SQLDatastore) Start() error {
	conn, err := gorm.Open("postgres", db.url)
	if err != nil {
		return err
	}
	// Migrate the schema
	conn.AutoMigrate(
		&letterbox.User{},
		&letterbox.UserKey{},
		&letterbox.Domain{},
		&letterbox.Mailbox{},
		&letterbox.EncryptedMessage{},
		&letterbox.DomainStat{},
	)
	conn.Model(&letterbox.Mailbox{}).AddForeignKey("domain_id", "domains(id)", "CASCADE", "CASCADE")
	conn.Model(&letterbox.EncryptedMessage{}).AddForeignKey("mailbox_id", "mailboxes(id)", "CASCADE", "CASCADE")

	db.DB = conn

	return nil
}

func (db *SQLDatastore) Stop() error {
	return db.DB.Close()
}

func wrapGormErr(err error) error {
	if err == nil {
		return nil
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return letterbox.ErrNotFound
	}

	return fmt.Errorf("%w: %v", letterbox.ErrBackendError, err)
}
