package letterbox_test

import (
	"errors"
	"testing"

	"github.com/alexedwards/argon2id"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestAddInitialAdminUser(t *testing.T) {
	tests := []struct {
		name    string
		params  *AddUserParams
		dbSetup test.DBOption
		wantErr bool
	}{
		{
			name: "Creates a new user",
			params: &AddUserParams{
				Username: "foo",
				Password: "asdf",
				Email:    "foo@example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: false,
		},
		{
			name: "Fails if user validation fails",
			params: &AddUserParams{
				Username: "foo",
				// Password: "No password provided",
				Email: "foo@example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if users already exists",
			params: &AddUserParams{
				Username: "foo",
				Password: "asdf",
				Email:    "foo@example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{
					{
						Username: "bar",
						Password: "password",
						Email:    "bar@example.com",
					},
				}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if users can't be retrieved",
			params: &AddUserParams{
				Username: "foo",
				Password: "asdf",
				Email:    "foo@example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns(nil, errors.New("could not retrieve users"))
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if there's an error on save",
			params: &AddUserParams{
				Username: "foo",
				Password: "asdf",
				Email:    "foo@example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(errors.New("could not save user"))
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			db := test.DB(tt.dbSetup)
			hasher := ArgonHasher{*argon2id.DefaultParams}
			users := NewUserService(hasher, db, db)

			// Act
			err := users.AddInitialAdminUser(tt.params)

			// Assert
			if (err != nil) != tt.wantErr {
				t.Errorf("AddInitialAdminUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
