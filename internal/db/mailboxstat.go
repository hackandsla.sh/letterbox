package db

import "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"

func (db *SQLDatastore) GetStats(mailbox *letterbox.Mailbox) ([]letterbox.DomainStat, error) {
	stats := []letterbox.DomainStat{}
	if err := db.DB.Model(&mailbox).Order("count desc").Related(&stats).Error; err != nil {
		return nil, err
	}

	return stats, nil
}

func (db *SQLDatastore) GetStat(mailboxID uint, domain string) (*letterbox.DomainStat, error) {
	stat := &letterbox.DomainStat{}
	q := &letterbox.DomainStat{
		MailboxID: mailboxID,
		Domain:    domain,
	}

	if err := db.DB.Where(q).First(stat).Error; err != nil {
		return nil, wrapGormErr(err)
	}

	return stat, nil
}

func (db *SQLDatastore) UpdateStat(stat *letterbox.DomainStat) error {
	return wrapGormErr(db.DB.Save(stat).Error)
}

func (db *SQLDatastore) CreateStat(stat *letterbox.DomainStat) error {
	return wrapGormErr(db.DB.Create(stat).Error)
}
