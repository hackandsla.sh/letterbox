package test

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/kennygrant/sanitize"
	"github.com/kylelemons/godebug/diff"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

var (
	ErrPathDiscovery = errors.New("could not identify test objects path")
)

func GetTestKey() (*letterbox.UserKey, error) {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil, ErrPathDiscovery
	}

	testKeyDir := path.Join(path.Dir(filename), "key")
	pubKeyFile := path.Join(testKeyDir, "test_pub.pem")
	privKeyFile := path.Join(testKeyDir, "test.pem")

	pubKey, err := ioutil.ReadFile(pubKeyFile)
	if err != nil {
		return nil, err
	}

	privKey, err := ioutil.ReadFile(privKeyFile)
	if err != nil {
		return nil, err
	}

	return &letterbox.UserKey{
		PrivateKey: string(privKey),
		PublicKey:  string(pubKey),
	}, nil
}

func GetTestMessage(name string) io.Reader {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		panic("could not identify test objects path")
	}

	filename := path.Join(path.Dir(currentFile), "example_emails", name+".txt")

	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	return f
}

func assertStringEqual(t *testing.T, a, b string) {
	if a != b {
		t.Errorf("Expected strings to match, but got diff:\n%v", diff.Diff(a, b))
	}
}

func AssertMatchesGoldenFile(t *testing.T, got, name, extension string, update *bool) {
	filename := sanitize.Path(name) + extension
	file := filepath.Join("testdata", filename)

	if *update {
		parent := filepath.Dir(file)

		if _, err := os.Stat(parent); os.IsNotExist(err) {
			if err := os.MkdirAll(parent, 0777); err != nil {
				t.Fatalf("error creating directory '%s': %v", parent, err)
			}
		}

		if err := ioutil.WriteFile(file, []byte(got), 0600); err != nil {
			t.Fatalf("error updating golden file '%s': %v", file, err)
		}
	}

	var want string

	if _, err := os.Stat(file); err == nil {
		wantBytes, err := ioutil.ReadFile(file)
		if err != nil {
			t.Fatalf("error reading golden file '%s': %v", file, err)
		}

		want = string(wantBytes)
	}

	assertStringEqual(t, want, got)
}
