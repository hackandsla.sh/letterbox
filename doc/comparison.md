# Comparison with Existing Projects

## Summary

|      Service       | Custom Domain | Custom Email | Self-hosting  |    API     | Forwarding |     Free      | Authentication | Perpetual Access | Open Source |
| :----------------: | :-----------: | :----------: | :-----------: | :--------: | :--------: | :-----------: | :------------: | :--------------: | :---------: |
|        Blur        |               |              |               | :fa-check: | :fa-check: |  :fa-check:   |   :fa-check:   |    :fa-check:    |             |
|   10 Minute Mail   |               |              |               |            |            |  :fa-check:   |                |                  |             |
|      Mailsac       |  :fa-check:   |  :fa-check:  | :fa-check:^1^ | :fa-check: |            |               |   :fa-check:   |    :fa-check:    |             |
|    Burner Kiwi     |  :fa-check:   |              |  :fa-check:   |            |            |  :fa-check:   |                |                  | :fa-check:  |
|      Inboxen       |
|      MailCare      |
|    Inbox Kitten    |
|   please no spam   |
|    Disposeamail    |
| Disposable Mailbox |
|      ImprovMX      |  :fa-check:   |  :fa-check:  |               | :fa-check: | :fa-check: | :fa-check:^2^ |   :fa-check:   |    :fa-check:    |             |
|        nBox        |               |              |               |            |            |  :fa-check:   |   :fa-check:   |    :fa-check:    |             |
|      AnonAddy      |  :fa-check:   |  :fa-check:  |  :fa-check:   | :fa-check: | :fa-check: | :fa-check:^3^ |   :fa-check:   |    :fa-check:    | :fa-check:  |

*[1]: only self-hosted with a business license
*[2]: free account is limited
*[3]: free account is limited, but generous

## [Abine Blur](https://www.abine.com/index.html)

### Pros

- Offers email forwarding to a registered email.
- Allows permanent access to emails.
- Allows easy turning on/off of forwarding emails, while still receiving all the emails sent to the emails.
- Web app, mobile app, and browser extensions can be used to create and configure the proxy emails.
- Each forwarded email includes a way to turn off forwarding via an embedded link.
- Free tier includes email forwarding.

### Cons

- Not self-hosted
- No custom domains
- No custom emails
- No API

**Overall Review**: Very solid service and workflow that's fantastic other than its customizability. Because you can't own any of the domains used for email, you basically have to trust that you don't lose access to the service if you want to use the proxy emails.

## [10 Minute Mail](https://10minutemail.com)

### Pros

- Free
- Simple interface

### Cons

- 10 minute maximum email lifespan; no permanent access
- Not self-hosted
- No custom domains
- No custom emails
- No outbound email
- No API

**Overall Review**: Very fast an intuitive service for simple temporary email tasks (like signing up for an email list), but not so great for any kind of account creation.

## [Mailsac](https://mailsac.com)

### Pros

- Free for basic temporary inbound email
- API driven
- Wide array of temporary email-related features
- Supports custom domains

### Cons

- Self-hosting requires a business license
- Costs add up quick -- You'll be paying at least \$75/year for a custom domain, plus costs for API keys and outbound email
- No out-of-the-box email proxying

**Overall Review**: The platform seems good for building apps on top of, but gets very expensive very quickly.

## [MailCare](https://gitlab.com/mailcare/mailcare)

_TODO_

## [Inboxen](https://github.com/Inboxen/Inboxen)

_TODO_

## [Burner Kiwi](https://burner.kiwi/)

### Pros

- API-first architecture
- Simple UI
- Self-hostable and SaaS
- Supports (limited) authentication
- Mailgun support
- Easy AWS or manual deployment
- Custom domains

### Cons

- No custom emails - all appear to be randomly generated even for custom domains
- Authentication does not prevent arbitrary access to create inboxes; other methods would have to be used to control access

### Overall Review

For truly temporary emails, this seems to be the best lightweight solution.

### [Inbox Kitten](https://github.com/uilicious/inboxkitten)

### [please no spam](https://github.com/JoeBiellik/pleasenospam)

### [Disposeamail](https://disposeamail.com/)

### [Disposable Mailbox](https://github.com/synox/disposable-mailbox)

### [ImprovMX](https://improvmx.com/)

### [nBox](https://nbox.notif.me/)

### Cons

- Doens't have hard filters for mailboxes attached to specific services. Although you can attach a mailbox to a server (e.g. `https://www.facebook.com`), that mailbox will still accept any mail and display it. It appears the feature is solely asthetic, where it will display the service name in the alert message.

