package httpserver

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"sync/atomic"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type statusError struct {
	msg    string
	status int
}

func (se *statusError) Error() string {
	return se.msg
}

type statusResponse struct {
	Status  string `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
}

func successResponse() statusResponse {
	return statusResponse{
		Status: "success",
	}
}

func errorResponse(err error) statusResponse {
	return statusResponse{
		Status:  "error",
		Message: err.Error(),
	}
}

func getStatusCodeForError(err error) int {
	var statusCodeMap map[error]int = map[error]int{
		letterbox.ErrNotFound:        http.StatusNotFound,
		letterbox.ErrAlreadyExists:   http.StatusBadRequest,
		letterbox.ErrValidationError: http.StatusBadRequest,
		ErrUnauthorized:              http.StatusUnauthorized,
		ErrDecode:                    http.StatusBadRequest,
	}

	for e, code := range statusCodeMap {
		if errors.Is(err, e) {
			return code
		}
	}

	return http.StatusInternalServerError
}

// HandlerFunc describes our custom HTTP handler definition. It's similar to a
// standard http.HandlerFunc, except it also allows returning an interface{} and
// an error.
type HandlerFunc func(http.ResponseWriter, *http.Request) (interface{}, error)

// Handler wraps a HandlerFunc with a logger to provide consistent logging.
type Handler struct {
	Handler HandlerFunc
	log     logger.Logger
}

// ServeHTTP will call the wrapped HandlerFunc. If the returned interface{} is
// non-nil, it is to be the response body. If error is non-nil, an HTTP error
// message is to be returned. See getStatusCodeForError to determine how
// specific errors are mapped to HTTP status codes.
func (a Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	responseObject, err := a.Handler(w, r)
	if err != nil {
		var statusErr *statusError
		if !errors.Is(err, statusErr) {
			statusErr = &statusError{
				status: getStatusCodeForError(err),
				msg:    err.Error(),
			}
		}

		// ? Should this logging be done via a separate middleware?
		a.log.InfoErr("status error", err,
			logger.Fields{
				"code": statusErr.status,
			},
		)

		statusMessage := errorResponse(err)

		err := respondJSON(w, statusMessage, statusErr.status)
		if err != nil {
			a.log.Errorw("could not write out error message", err)
		}

		return
	}

	if responseObject != nil {
		_ = respondJSON(w, responseObject, http.StatusOK)
	}
}

func (s *HTTPService) handleUnauthorized() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		w.Header().Add("WWW-Authenticate", `Basic realm="letterbox"`)
		return nil, ErrUnauthorized
	}
}

// handleHello returns basic server information.
func (s *HTTPService) handleHello() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		return s.serverInfo, nil
	}
}

func (s *HTTPService) handleHealthz() HandlerFunc {
	type Ping struct {
		Status string `json:"status"`
	}

	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		if atomic.LoadInt32(&s.isHealthy) == 0 {
			return nil, ErrServiceUnhealthy
		}

		return Ping{Status: "up"}, nil
	}
}

func (s *HTTPService) handleGetAllMailboxes() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")

		domain, err := s.domains.GetDomain(paramDomain)
		if err != nil {
			return nil, err
		}

		m, err := s.db.GetMailboxesByDomain(domain)
		if err != nil {
			return nil, fmt.Errorf("can't retrieve mailboxes for domain '%s': %w", paramDomain, err)
		}

		return m, nil
	}
}

func (s *HTTPService) handleGetOneMailbox() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := s.getMailboxByURLPath(r)
		if err != nil {
			return nil, err
		}

		return mailbox, nil
	}
}

func (s *HTTPService) handleDeleteOneMailbox() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		params := getVars(r)
		paramDomain := params.ByName("domain")
		paramName := params.ByName("name")

		if err := s.mailboxes.DeleteMailbox(paramDomain, paramName); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleUpsertMailbox() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		params := getVars(r)
		paramDomain := params.ByName("domain")
		paramName := params.ByName("name")

		user, err := getAuthenticatedUser(r)
		if err != nil {
			return nil, err
		}

		var body letterbox.MailboxParams
		if err := s.decodeJSONBody(w, r, &body); err != nil {
			// Allow empty bodies
			if !errors.Is(err, io.EOF) {
				return nil, err
			}
		}

		if err := s.mailboxes.SetMailbox(paramDomain, paramName, user.ID, &body); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handlePatchMailbox() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		params := getVars(r)
		paramDomain := params.ByName("domain")
		paramName := params.ByName("name")

		var body letterbox.MailboxParams
		if err := s.decodeJSONBody(w, r, &body); err != nil {
			// Allow empty bodies
			if !errors.Is(err, io.EOF) {
				return nil, err
			}
		}

		if err := s.mailboxes.PatchMailbox(paramDomain, paramName, &body); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleDisableMailbox() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramID := getVar(r, "id")

		if err := s.mailboxes.DisableMailboxByExternalID(paramID); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleGetMessages() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := s.getMailboxByURLPath(r)
		if err != nil {
			return nil, err
		}

		user, err := getAuthenticatedUser(r)
		if err != nil {
			return nil, err
		}

		_, password, _ := r.BasicAuth()

		messages, err := s.messages.GetMessages(mailbox, user, password)
		if err != nil {
			return nil, fmt.Errorf("error retrieving messages for mailbox '%s': %w", mailbox.Address, err)
		}

		return messages, nil
	}
}

func (s *HTTPService) handleGetMailboxStats() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := s.getMailboxByURLPath(r)
		if err != nil {
			return nil, err
		}

		stats, err := s.statsSummary.GetStats(mailbox)
		if err != nil {
			return nil, fmt.Errorf("error retrieving stats for mailbox '%s': %w", mailbox.Address, err)
		}

		return stats, nil
	}
}

func (s *HTTPService) getMailboxByURLPath(r *http.Request) (*letterbox.Mailbox, error) {
	params := getVars(r)
	paramDomain := params.ByName("domain")
	paramName := params.ByName("name")

	mailbox, err := s.mailboxes.GetMailbox(paramDomain, paramName)
	if err != nil {
		return nil, err
	}

	return mailbox, nil
}
