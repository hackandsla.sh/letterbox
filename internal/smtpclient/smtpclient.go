package smtpclient

import (
	"fmt"

	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gopkg.in/gomail.v2"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

const tcpMax = 65535

type Config struct {
	Host     string `json:"host,omitempty"`
	Port     int    `json:"port,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Host, is.Host),
		validation.Field(&c.Port, validation.Min(1), validation.Max(tcpMax)),
	)
}

type SMTPClient struct {
	log    logger.Logger
	dialer *gomail.Dialer
}

func New(config *Config, log logger.Logger) (*SMTPClient, error) {
	dialer := gomail.NewDialer(
		config.Host,
		config.Port,
		config.Username,
		config.Password,
	)

	closer, err := dialer.Dial()
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrFailedInit, err)
	}

	defer closer.Close()

	log.Infow("initiated SMTP client connection",
		logger.Fields{
			"host": config.Host,
			"port": config.Port,
			"user": config.Username,
		},
	)

	return &SMTPClient{
		dialer: dialer,
		log:    log,
	}, nil
}

func (sc *SMTPClient) SendMail(message *letterbox.ForwardingMessage) error {
	gomailMessage := newGomailMessage(message)
	if err := sc.dialer.DialAndSend(gomailMessage); err != nil {
		return fmt.Errorf("%w: %v", ErrFailedSend, err)
	}

	sc.log.Infow("successfully proxied email",
		logger.Fields{
			"proxyTo":   message.ToHeader,
			"proxyFrom": message.FromHeader,
		},
	)

	return nil
}

func newGomailMessage(m *letterbox.ForwardingMessage) *gomail.Message {
	ret := gomail.NewMessage()
	ret.SetHeader("From", m.FromHeader)
	ret.SetHeader("To", m.ToHeader)
	ret.SetHeader("Subject", m.Subject)

	if m.TextBody != "" && m.HTMLBody != "" {
		ret.SetBody("text/plain", m.TextBody)
		ret.AddAlternative("text/html", m.HTMLBody)
	} else if m.TextBody != "" {
		ret.SetBody("text/plain", m.TextBody)
	} else if m.HTMLBody != "" {
		ret.SetBody("text/html", m.HTMLBody)
	}

	return ret
}
