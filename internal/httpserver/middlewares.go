package httpserver

import (
	"context"
	"net"
	"net/http"
	"strings"

	"github.com/jaredfolkins/badactor"
	metrics "github.com/slok/go-http-metrics/metrics/prometheus"
	"github.com/slok/go-http-metrics/middleware"
	"github.com/unrolled/secure"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type key int

const (
	AuthenticatedUserKey key = iota + 1
)

type Middleware func(http.Handler) http.Handler

func wrapMiddleware(handler http.Handler, middlewares ...Middleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m(newHandler)
	}

	return newHandler
}

// PerRouteMiddleware adds middleware that requires information about the specific
// paths used (e.g. HTTP metric collection, which needs to know what path a given
// function is registered to).
type PerRouteMiddleware interface {
	AddMiddleware(path string, handler http.Handler) http.Handler
}

func wrapPerRouteMiddleware(path string, handler http.Handler, middlewares ...PerRouteMiddleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m.AddMiddleware(path, newHandler)
	}

	return newHandler
}

type endpointMetricsMiddleware struct {
	MetricsCollector middleware.Middleware
}

func newMetricsMiddleware() *endpointMetricsMiddleware {
	return &endpointMetricsMiddleware{
		MetricsCollector: middleware.New(middleware.Config{
			Recorder: metrics.NewRecorder(metrics.Config{}),
		})}
}

func (m *endpointMetricsMiddleware) AddMiddleware(path string, handler http.Handler) http.Handler {
	return m.MetricsCollector.Handler(path, handler)
}

func authMiddleware(s *HTTPService) Middleware {
	return Middleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			presentLogin := wrapHandler(s.handleUnauthorized(), s.log)
			username, password, authOK := r.BasicAuth()
			if !authOK {
				presentLogin.ServeHTTP(w, r)
				return
			}

			remoteIP, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				s.log.InfoErr("could not get remote IP", err)
				presentLogin.ServeHTTP(w, r)
				return
			}
			log := s.log.With(
				logger.Fields{
					"ip":   remoteIP,
					"user": username,
				},
			)

			user, err := s.db.GetUser(username)
			if err != nil {
				if infractionErr := s.badActors.Infraction(remoteIP, "Login"); infractionErr != nil {
					log.Errorw("error while adding badactor infraction", infractionErr)
				}
				log.InfoErr("invalid username attempted", err)
				presentLogin.ServeHTTP(w, r)
				return
			}

			valid, err := s.hasher.Validate(password, user.Password)
			if err != nil {
				log.InfoErr("error while looking up password", err)
				presentLogin.ServeHTTP(w, r)
				return
			}

			if !valid {
				if infractionErr := s.badActors.Infraction(remoteIP, "Login"); infractionErr != nil {
					log.Errorw("error while adding badactor infraction", err)
				}
				log.Infow("invalid password attempted")
				presentLogin.ServeHTTP(w, r)
				return
			}
			log.Debugw("login succeeded")
			r = addValueToContext(r, AuthenticatedUserKey, user)
			next.ServeHTTP(w, r)
		})
	})
}

func getAuthenticatedUser(r *http.Request) (*letterbox.User, error) {
	user, ok := r.Context().Value(AuthenticatedUserKey).(*letterbox.User)
	if !ok {
		return nil, ErrUnauthorized
	}

	return user, nil
}

func newBadActorMiddleware(studio *badactor.Studio, log logger.Logger) Middleware {
	return Middleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			remoteIP, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				log.InfoErr("could not get remote IP", err)
				next.ServeHTTP(w, r)
			}
			if studio.IsJailed(remoteIP) {
				http.Error(w, "IP is currently banned due to failed login attempts", http.StatusForbidden)
				return
			}
			next.ServeHTTP(w, r)
		})
	})
}

func addValueToContext(r *http.Request, key key, value interface{}) *http.Request {
	ctx := context.WithValue(r.Context(), key, value)
	return r.WithContext(ctx)
}

func newSecurityMiddleware() Middleware {
	featurePolicies := []string{
		"geolocation none",
		"midi none",
		"notifications none",
		"push none",
		"sync-xhr none",
		"microphone none",
		"camera none",
		"magnetometer none",
		"gyroscope none",
		"speaker self",
		"vibrate none",
		"payment none",
	}
	s := secure.New(secure.Options{
		FrameDeny:          true,
		BrowserXssFilter:   true,
		ContentTypeNosniff: true,
		ReferrerPolicy:     "same-origin",
		FeaturePolicy:      strings.Join(featurePolicies, ";"),
	})

	return s.Handler
}
