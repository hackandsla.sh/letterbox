APP = ./letterbox
SHELL = bash
include .bingo/Variables.mk
export

build:
	go build -o $(APP)
	sudo setcap 'cap_net_bind_service=+ep' $(APP)

start-dev-server: $(COMPILEDAEMON)
	${COMPILEDAEMON} -command="./letterbox" -build="make build"

dev:
	docker-compose up -d
	docker-compose logs -f webapp 

dev-refresh:
	docker-compose down

build-ui: $(SWAGGER) $(PKGER)
	$(SWAGGER) generate spec -o api/swagger.yaml
	redoc-cli bundle -o static/doc/index.html api/swagger.yaml
	$(PKGER)

ca: $(CFSSL)
	$(CFSSL) gencert -initca test/cert/ca-csr.json | cfssljson -bare test/cert/ca -

cert: ca
	$(CFSSL) gencert -ca=test/cert/ca.pem -ca-key=test/cert/ca-key.pem -config=test/cert/ca-config.json -profile=server test/cert/server.json | cfssljson -bare test/cert/server

remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|master)" | xargs git branch -d

gen: $(COUNTERFEITER) $(GO_ENUM)
	go generate ./...

test: gen
	go test ./...

lint: $(GOLANGCI_LINT)
	$(GOLANGCI_LINT) run ./... -v --timeout 5m

tidy: 
	./build/tidy.sh

pull-master:
	git checkout master
	git pull

release: pull-master test lint tidy
	standard-version

install-dev-tools: $(BINGO)
	$(BINGO) get

lsif: $(LSIF_GO)
	$(LSIF_GO)

goreleaser-test: $(GORELEASER)
	$(GORELEASER) build --rm-dist --snapshot && rm -rf ./dist

junit: $(GO_JUNIT_REPORT) 
	go test -covermode=count -coverprofile=coverage.txt ./... -v 2>&1 | $(GO_JUNIT_REPORT) -set-exit-code > report.xml
	go tool cover -html=coverage.txt -o index.html
	go tool cover -func=coverage.txt

.PHONY: build
