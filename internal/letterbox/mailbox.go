package letterbox

import (
	"fmt"
	"math"
	"regexp"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

const (
	proxyAddressLength = 20
	externalIDLength   = 30
)

// MailboxStore defines the persistence and retrieval mechanisms for
// mailboxes.
type MailboxStore interface {
	GetMailboxByExternalID(id string) (*Mailbox, error)
	UpdateMailbox(m *Mailbox) error
	GetMailboxesByDomain(domain *Domain) ([]Mailbox, error)
	GetMailboxByDomain(domain *Domain, name string) (*Mailbox, error)
	GetAllMailboxes() ([]Mailbox, error)
	GetExpiredMailboxes() ([]Mailbox, error)
	DeleteMailbox(m *Mailbox) error
}

type MailboxService struct {
	db      MailboxStore
	domains *DomainService
}

func NewMailboxService(db MailboxStore, domains *DomainService) *MailboxService {
	return &MailboxService{
		db:      db,
		domains: domains,
	}
}

// Mailbox represents all the relevant information about a particular mailbox.
type Mailbox struct {
	gorm.Model `json:"-"`
	// DomainID is the reference to the domain of this address
	DomainID uint `json:"-"`

	// UserID is the reference to the user that owns this mailbox
	UserID uint `json:"-"`

	// Name is the name of this mailbox. This is the first part of the associated email address
	Name string `gorm:"type:varchar(100)" json:"name,omitempty"`

	// Address holds the full email address of this mailbox
	Address string `json:"address,omitempty"`

	// ProxyAddress is the return address used in forwarded emails
	ProxyAddress string `json:"-"`

	// Enabled indicates whether this mailbox is enabled or not. If not, then
	// all mails for this address are rejected similarly to if this mailbox
	// didn't exist.
	Enabled bool `json:"enabled"`

	// Expires indicates an absolute (RFC3339) time for this mailbox to expire. If this is set,
	// the mailbox will be deleted after the designated time.
	Expires *time.Time `json:"expires,omitempty"`

	// DomainWhitelist contains an array of domains to match against. If this is
	// set, the "from" header for all mail destined for this mailbox must match
	// one of the domains listed in this whitelist. Whitelist domains may also
	// be wildcards, with '*' matching multiple characters and '?' matching
	// single characters.
	DomainWhitelist pq.StringArray `gorm:"type:varchar(100)[]" json:"domainWhitelist,omitempty"`

	// ExternalID is a randomly-generated ID which can be used to reference this
	// mailbox from unauthenticated contexts, such as the "disable mailbox"
	// endpoint.
	ExternalID string `json:"-"`
}

// newMailbox constructs a default mailbox.
func newMailbox(domain, name string, domainID, userID uint) (*Mailbox, error) {
	externalID, err := GetRandomString(externalIDLength)
	if err != nil {
		return nil, wrapErr(ErrBackendError, err)
	}

	proxyAddressPrefix, err := GetRandomString(proxyAddressLength)
	if err != nil {
		return nil, wrapErr(ErrBackendError, err)
	}

	mailbox := &Mailbox{
		ProxyAddress: fmt.Sprintf("%s@%s", proxyAddressPrefix, domain),
		ExternalID:   externalID,
		Address:      fmt.Sprintf("%s@%s", name, domain),
		Name:         name,
		DomainID:     domainID,
		UserID:       userID,
	}

	return mailbox.withDefaults(), nil
}

func (m Mailbox) withDefaults() *Mailbox {
	m.Enabled = true
	m.Expires = nil
	m.DomainWhitelist = nil

	return &m
}

func (m Mailbox) withPatch(params *MailboxParams) *Mailbox {
	m.Enabled = getBoolPOrDefault(params.Enabled, m.Enabled)
	m.Expires = getTimePOrDefault(params.Expires, m.Expires)
	m.DomainWhitelist = getStringSliceOrDefault(params.DomainWhitelist, m.DomainWhitelist)

	return &m
}

type MailboxParams struct {
	// Enabled indicates whether this mailbox is enabled or not. If not, then
	// all mail for this address is rejected similarly to if this mailbox
	// didn't exist.
	Enabled *bool `json:"enabled,omitempty"`

	// Expires indicates an absolute (RFC3339) time for this mailbox to expire. If this is set,
	// the mailbox will be deleted after the designated time.
	Expires *time.Time `json:"expires,omitempty"`

	// DomainWhitelist contains an array of domains to match against. If this is
	// set, the "from" header for all mail destined for this mailbox must match
	// one of the domains listed in this whitelist. Whitelist domains may also
	// be wildcards, with '*' matching multiple characters and '?' matching
	// single characters.
	DomainWhitelist []string `json:"domainWhitelist,omitempty"`
}

func (params *MailboxParams) Validate() error {
	return validation.ValidateStruct(params,
		validation.Field(&params.Expires, validation.By(validateTimeAfterNow)),
		validation.Field(&params.DomainWhitelist, validation.Each(validation.By(isWildcardableDomain))),
	)
}

// GetMailbox gets the mailbox with the given domain and name.
func (ms *MailboxService) GetMailbox(domain, name string) (*Mailbox, error) {
	d, err := ms.domains.GetDomain(domain)
	if err != nil {
		return nil, err
	}

	m, err := ms.db.GetMailboxByDomain(d, name)
	if err != nil {
		return nil, fmt.Errorf("mailbox '%s' in domain '%s': %w", name, domain, ErrNotFound)
	}

	return m, nil
}

// SetMailbox idempotently upserts a mailbox to a specific domain.
func (ms *MailboxService) SetMailbox(domain, name string, userID uint, params *MailboxParams) error {
	if name == "" {
		return fmt.Errorf("%w: 'name' cannot be empty", ErrInvalidValue)
	}

	if err := params.Validate(); err != nil {
		return err
	}

	d, err := ms.domains.GetDomain(domain)
	if err != nil {
		return err
	}

	mailbox, err := ms.db.GetMailboxByDomain(d, name)
	if itemNotFound(err) {
		mailbox, err = newMailbox(d.Domain, name, d.ID, userID)
	}

	if err != nil {
		return fmt.Errorf("error adding mailbox '%s' to domain '%s': %w", name, d.Domain, err)
	}

	mailbox = mailbox.withDefaults().withPatch(params)

	if err := ms.db.UpdateMailbox(mailbox); err != nil {
		return err
	}

	return nil
}

// PatchMailbox updates the values of an existing mailbox.
func (ms *MailboxService) PatchMailbox(domain, name string, params *MailboxParams) error {
	if err := params.Validate(); err != nil {
		return err
	}

	d, err := ms.domains.GetDomain(domain)
	if err != nil {
		return err
	}

	mailbox, err := ms.db.GetMailboxByDomain(d, name)
	if err != nil {
		return err
	}

	mailbox = mailbox.withPatch(params)

	if err := ms.db.UpdateMailbox(mailbox); err != nil {
		return err
	}

	return nil
}

// DeleteMailbox deletes the mailbox with the given name and domain.
func (ms *MailboxService) DeleteMailbox(domain, name string) error {
	d, err := ms.domains.GetDomain(domain)
	if err != nil {
		return err
	}

	mailbox, err := ms.db.GetMailboxByDomain(d, name)
	if err != nil {
		return err
	}

	return ms.db.DeleteMailbox(mailbox)
}

// DeleteExpiredMailboxes deletes all mailboxes where the current time is past
// their expiry time.
func (ms *MailboxService) DeleteExpiredMailboxes(log logger.Logger) error {
	expiredMailboxes, err := ms.db.GetExpiredMailboxes()
	if err != nil {
		return err
	}

	for i := 0; i < len(expiredMailboxes); i++ {
		mailbox := &expiredMailboxes[i]

		log.Infow("cleaning up mailbox",
			logger.Fields{
				"address": mailbox.Address,
			},
		)

		if err := ms.db.DeleteMailbox(mailbox); err != nil {
			return err
		}
	}

	return nil
}

// DisableMailboxByExternalID disables a particular mailbox given its external
// identifier. This may be called from unauthenticated contexts.
func (ms *MailboxService) DisableMailboxByExternalID(id string) error {
	m, err := ms.db.GetMailboxByExternalID(id)
	if err != nil {
		return fmt.Errorf("error searching for mailbox with external address '%s': %w", id, err)
	}

	m.Enabled = false
	if err := ms.db.UpdateMailbox(m); err != nil {
		return fmt.Errorf("error while disabling mailbox with external ID '%s': %w", id, err)
	}

	return nil
}

// GetOrCreateMailboxForAddress retreives a Mailbox object given an RFC822-formated
// email address.
func (ms *MailboxService) GetOrCreateMailboxForAddress(to string) (*Mailbox, error) {
	toAddr, err := SplitAddr(to)
	if err != nil {
		return nil, fmt.Errorf("%w: couldn't parse address '%s", ErrInvalidDestination, to)
	}

	domain, err := ms.domains.GetDomain(toAddr.Domain)
	if err != nil {
		return nil, fmt.Errorf("%w: no domain found matching '%s'", ErrInvalidDestination, toAddr.Domain)
	}

	mailbox, err := ms.db.GetMailboxByDomain(domain, toAddr.Username)
	if itemNotFound(err) && domain.CatchallEnabled {
		mailbox, err = newMailbox(domain.Domain, toAddr.Username, domain.ID, domain.UserID)
		if err != nil {
			return nil, err
		}

		if err1 := ms.db.UpdateMailbox(mailbox); err1 != nil {
			return nil, err1
		}

		return ms.db.GetMailboxByDomain(domain, toAddr.Username)
	}

	if err != nil || !mailbox.Enabled {
		return nil, fmt.Errorf("%w: no username found matching '%s'", ErrInvalidDestination, toAddr.Username)
	}

	return mailbox, nil
}

func validateTimeAfterNow(value interface{}) error {
	t, ok := value.(*time.Time)
	if !ok {
		return fmt.Errorf("%w: value of type %T is not *time.Time", ErrInvalidValue, value)
	}

	if t != nil && t.Before(time.Now()) {
		return fmt.Errorf("%w: must be set to a future time", ErrInvalidValue)
	}

	return nil
}

// This is based on ozzo-validation's is.Domain, but slightly tweaked to allow either normal segments or '*'.
var reWildcardableDomain = regexp.MustCompile(
	`^(?:([a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-z0-9])?|\*)\.)+(?:[a-zA-Z]{1,63}| xn--[a-z0-9]{1,59})$`,
)

func isWildcardableDomain(value interface{}) error {
	domain, ok := value.(string)
	if !ok {
		return fmt.Errorf("%w: given value is not a string", ErrInvalidValue)
	}

	if len(domain) > math.MaxUint8 {
		return fmt.Errorf("%w: domains must be less than 255 characters", ErrInvalidValue)
	}

	if !reWildcardableDomain.Match([]byte(domain)) {
		return fmt.Errorf("%w: domain must be a standard domain name with optional '*' characters indicating wildcard", ErrInvalidValue)
	}

	return nil
}
