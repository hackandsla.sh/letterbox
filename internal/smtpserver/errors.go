package smtpserver

import "errors"

var (
	ErrHashFail           = errors.New("error while hashing password")
	ErrInvalidDestination = errors.New("invalid destination")
	ErrParsingError       = errors.New("parsing error")
	ErrDomainMisalignment = errors.New("domains not aligned")
	ErrMaxSizeExceeded    = errors.New("maximum message size exceeded")
	ErrValidationError    = errors.New("validation failed")
	ErrMissingDependency  = errors.New("missing dependency")
)
