package letterbox

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

// DomainStat represents a count of unique sources we've seen send mail to this
// mailbox.
type DomainStat struct {
	gorm.Model `json:"-"`
	MailboxID  uint   `json:"-"`
	Domain     string `json:"domain,omitempty"`
	Count      uint   `json:"count,omitempty"`
}

func newDomainStat(domain string, mailboxID uint) *DomainStat {
	return &DomainStat{
		MailboxID: mailboxID,
		Domain:    domain,
		Count:     1,
	}
}

// MailboxStatStore defines our persistence and retrieval mechaisms for
// DomainsStat.
type MailboxStatStore interface {
	GetStat(mailboxID uint, domain string) (*DomainStat, error)
	UpdateStat(stat *DomainStat) error
	CreateStat(stat *DomainStat) error
}

type MailboxStatsService struct {
	statsDB MailboxStatStore
}

func NewMailboxStatsService(statsDB MailboxStatStore) *MailboxStatsService {
	return &MailboxStatsService{
		statsDB: statsDB,
	}
}

// IncrementOrInsertStat upserts a new statistic to indicate that another email
// from the given source domain has be received.
func (mss *MailboxStatsService) IncrementOrInsertStat(mailboxID uint, domain string) error {
	stat, err := mss.statsDB.GetStat(mailboxID, domain)
	if itemNotFound(err) {
		stat = newDomainStat(domain, mailboxID)

		return mss.statsDB.CreateStat(stat)
	}

	if err != nil {
		return fmt.Errorf("could not get domain stats: %w: %v", ErrBackendError, err)
	}

	stat.Count++

	return mss.statsDB.UpdateStat(stat)
}
