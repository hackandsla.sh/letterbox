package smtpserver_test

import (
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/docker/go-units"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	. "gitlab.com/hackandsla.sh/letterbox/internal/smtpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/smtpserverfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestSession_Rcpt(t *testing.T) {
	type args struct {
		to string
	}

	tests := []struct {
		name    string
		dbSetup func(db *letterboxfakes.FakeDatastore)
		args    args
		wantErr bool
	}{
		{
			name: "Gets a valid email",
			args: args{
				to: "foo@example.com",
			},
			wantErr: false,
		},
		{
			name: "Domain doesn't exist",
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, errors.New("no domain exists"))
			},
			args: args{
				to: "foo@example.com",
			},
			wantErr: true,
		},
		{
			name: "Email is invalid",
			args: args{
				to: "foo@example.com@example.com",
			},
			wantErr: true,
		},
		{
			name: "Email doesn't have domain",
			args: args{
				to: "foo",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			db := test.DB(tt.dbSetup)

			s := getTestSMTPSession(
				withDB(db),
			)

			if err := s.Rcpt(tt.args.to); (err != nil) != tt.wantErr {
				t.Errorf("Session.Rcpt() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type ReaderError struct{}

func (r *ReaderError) Read(p []byte) (n int, err error) {
	return 0, errors.New("could not read document")
}

type FakeAddr struct{}

func (a *FakeAddr) Network() string { return "" }
func (a *FakeAddr) String() string  { return "192.168.1.1:50000" }

func TestSession_Data(t *testing.T) {
	tests := []struct {
		name       string
		dbSetup    func(db *letterboxfakes.FakeDatastore)
		dbValidate func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions)
		message    io.Reader
		mailbox    *letterbox.Mailbox
		wantErr    bool
	}{
		{
			name:    "Adds email without Content-Type or multipart as plaintext",
			message: test.GetTestMessage("simple"),
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				assert.GreaterOrEqual(db.AddMessageCallCount(), 1)
			},
			wantErr: false,
		},
		{
			name:    "Adds multipart email with valid DKIM",
			message: test.GetTestMessage("google"),
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				assert.GreaterOrEqual(db.AddMessageCallCount(), 1)
			},
			wantErr: false,
		},
		{
			name:    "Adds multipart email content-encoding",
			message: test.GetTestMessage("base64"),
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				assert.GreaterOrEqual(db.AddMessageCallCount(), 1)
			},
			wantErr: false,
		},
		{
			name:    "Adds email if domain is on whitelist",
			message: test.GetTestMessage("google"),
			mailbox: test.Mailbox(test.WithDomainAllowlist("gmail.com")),
			wantErr: false,
		},
		{
			name:    "Rejects email if domain doesn't match a whitelist domain",
			message: test.GetTestMessage("google"),
			mailbox: test.Mailbox(test.WithDomainAllowlist("example.com")),
			wantErr: true,
		},
		{
			name:    "Add Message Failure",
			message: strings.NewReader("foobar"),
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.AddMessageReturns(errors.New("could not add messages"))
			},
			wantErr: true,
		},
		{
			name:    "Reader Failure",
			message: &ReaderError{},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			assert := assert.New(t)
			db := test.DB(tt.dbSetup)

			s := getTestSMTPSession(
				withDB(db),
			)

			if tt.mailbox != nil {
				s.Mailbox = tt.mailbox
			}

			// Act + Assert
			if err := s.Data(tt.message); (err != nil) != tt.wantErr {
				t.Errorf("Session.Data() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.dbValidate != nil {
				tt.dbValidate(db, assert)
			}
		})
	}
}

func TestSession_Data_MaxSize(t *testing.T) {
	tests := []struct {
		name    string
		maxSize int64
		wantErr bool
	}{
		{"Message too large", 100, true},
		{"Message size acceptable", 100 * units.KB, false},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			db := test.DB()

			s := getTestSMTPSession(
				withDB(db),
				withMaxSize(tt.maxSize),
			)

			r := test.GetTestMessage("google")

			if err := s.Data(r); (err != nil) != tt.wantErr {
				t.Errorf("Session.Data() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func getTestSMTPSession(options ...sessionOption) *Session {
	session := &Session{
		SMTPClient: &smtpserverfakes.FakeEmailSender{},
		DB:         test.DB(),
		MaxSize:    units.KB * 100,
		DKIM:       &smtpserverfakes.FakeDkimChecker{},
		SPF:        &smtpserverfakes.FakeSpfChecker{},
		Log:        test.Logger(),
		Banner:     test.BannerBuilder(),

		RemoteAddr: &FakeAddr{},
		Helo:       "example.com",
		Mailbox:    test.Mailbox(),
	}

	for _, option := range options {
		option(session)
	}

	session.Mailboxes = test.MailboxService(session.DB)
	session.Messages = letterbox.NewMessageService(session.Log, session.DB, session.DB, session.DB)
	session.Stats = letterbox.NewMailboxStatsService(session.DB)

	return session
}

type sessionOption func(*Session)

func withDB(db letterbox.Datastore) sessionOption {
	return func(s *Session) {
		s.DB = db
	}
}

func withMaxSize(size int64) sessionOption {
	return func(s *Session) {
		s.MaxSize = size
	}
}
