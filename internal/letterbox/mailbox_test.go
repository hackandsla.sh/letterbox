package letterbox_test

import (
	"errors"
	"testing"
	"time"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func boolP(b bool) *bool {
	return &b
}

func TestPatchMailbox(t *testing.T) {
	type args struct {
		domain string
		name   string
		params *MailboxParams
	}

	tests := []struct {
		name       string
		args       args
		wantErr    bool
		dbSetup    func(db *letterboxfakes.FakeDatastore)
		dbValidate func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions)
	}{
		{
			name: "Updates the enabled state on a mailbox",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Enabled: boolP(false),
				},
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				if db.UpdateMailboxCallCount() != 1 {
					assert.FailNow("UpdateMailbox() count incorrect", "expected count=%v, got %v", 1, db.UpdateMailboxCallCount())
				}

				mailbox := db.UpdateMailboxArgsForCall(0)
				assert.False(mailbox.Enabled)
			},
		},
		{
			name: "Adds an expiry time",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Expires: func() *time.Time {
						t := time.Now().Add(time.Hour)
						return &t
					}(),
				},
			},
			wantErr: false,
		},
		{
			name: "Adds a domain whitelist",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					DomainWhitelist: []string{"*.example.com"},
				},
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				if db.UpdateMailboxCallCount() != 1 {
					assert.FailNow("UpdateMailbox() count incorrect", "expected count=%v, got %v", 1, db.UpdateMailboxCallCount())
				}

				mailbox := db.UpdateMailboxArgsForCall(0)
				assert.Equal(pq.StringArray{"*.example.com"}, mailbox.DomainWhitelist)
			},
			wantErr: false,
		},
		{
			name: "Errors if the expiry time is in the past",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Expires: func() *time.Time {
						t := time.Now().Add(-1 * time.Hour)
						return &t
					}(),
				},
			},
			wantErr: true,
		},
		{
			name: "Errors if backend error occurs during update",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Enabled: boolP(false),
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.UpdateMailboxReturns(ErrBackendError)
			},
			wantErr: true,
		},
		{
			name: "Errors if domain doesn't exist",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Enabled: boolP(false),
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
		{
			name: "Errors if mailbox doesn't exist",
			args: args{
				domain: "example.com",
				name:   "foo",
				params: &MailboxParams{
					Enabled: boolP(false),
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert := assert.New(t)

			db := test.DB(tt.dbSetup)
			mailboxes := test.MailboxService(db)
			if err := mailboxes.PatchMailbox(tt.args.domain, tt.args.name, tt.args.params); (err != nil) != tt.wantErr {
				t.Errorf("TestPatchMailbox() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.dbValidate != nil {
				tt.dbValidate(db, assert)
			}
		})
	}
}

func TestSetMailbox(t *testing.T) {
	testTime := time.Now().Add(1 * time.Hour)

	type args struct {
		domain  string
		name    string
		mailbox *MailboxParams
	}

	tests := []struct {
		name       string
		args       args
		dbSetup    func(db *letterboxfakes.FakeDatastore)
		dbValidate func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions)
		wantErr    bool
	}{
		{
			name: "Adds a new mailbox to an existing domain given the mailbox doesn't already exist",
			args: args{
				domain:  "example.com",
				name:    "rick",
				mailbox: &MailboxParams{},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(nil, ErrNotFound)
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				if db.UpdateMailboxCallCount() != 1 {
					assert.FailNow("Wrong Call Count", "UpdateMailboxCallCount=%v, expected %v", db.UpdateMailboxCallCount(), 1)
				}

				msg := db.UpdateMailboxArgsForCall(0)
				assert.Equal("rick@example.com", msg.Address)
				assert.Regexp(`.{20}@example\.com`, msg.ProxyAddress)
				assert.True(msg.Enabled)
			},
		},
		{
			name: "Errors if the call to find existing mailboxes fails for any reason other than a nonexistent record",
			args: args{
				domain:  "example.com",
				name:    "rick",
				mailbox: &MailboxParams{},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(nil, ErrBackendError)
			},
			wantErr: true,
		},
		{
			name: "Overrides default values of the added message",
			args: args{
				domain: "example.com",
				name:   "rick",
				mailbox: &MailboxParams{
					Enabled:         boolP(false),
					DomainWhitelist: []string{"*.example.com"},
					Expires:         &testTime,
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(nil, ErrNotFound)
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				if db.UpdateMailboxCallCount() != 1 {
					assert.FailNow("Wrong Call Count", "UpdateMailboxCallCount=%v, expected %v", db.UpdateMailboxCallCount(), 1)
				}

				msg := db.UpdateMailboxArgsForCall(0)
				assert.False(msg.Enabled)
				assert.Equal(pq.StringArray{"*.example.com"}, msg.DomainWhitelist)
				wantTime := testTime.Round(time.Second)
				assert.Equal(&wantTime, msg.Expires)
			},
		},
		{
			name: "Errors if an invalid name is used",
			args: args{
				domain:  "example.com",
				name:    "",
				mailbox: &MailboxParams{},
			},
			wantErr: true,
		},
		{
			name: "Errors if the call to save the message fails",
			args: args{
				domain:  "example.com",
				name:    "rick",
				mailbox: &MailboxParams{},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.UpdateMailboxReturns(errors.New("couldn't add message"))
			},
			wantErr: true,
		},
		{
			name: "Errors if the domain can't be found",
			args: args{
				domain:  "example.com",
				name:    "rick",
				mailbox: &MailboxParams{},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert := assert.New(t)

			db := test.DB(tt.dbSetup)

			var userID uint = 123
			mailboxes := test.MailboxService(db)
			if err := mailboxes.SetMailbox(tt.args.domain, tt.args.name, userID, tt.args.mailbox); (err != nil) != tt.wantErr {
				t.Errorf("SetMailbox() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.dbValidate != nil {
				tt.dbValidate(db, assert)
			}
		})
	}
}

func Test_deleteExpiredMailboxes(t *testing.T) {
	Assert := assert.New(t)
	tests := []struct {
		name       string
		getDB      func() *letterboxfakes.FakeDatastore
		validateDB func(db *letterboxfakes.FakeDatastore)
		wantErr    bool
	}{
		{
			name: "Should delete the expired mailboxes",
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				now := time.Now()
				db.GetExpiredMailboxesReturns([]Mailbox{
					{
						Name:    "foo",
						Expires: &now,
					},
				}, nil)
				return db
			},
			validateDB: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(1, db.DeleteMailboxCallCount())
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := tt.getDB()
			mailboxes := test.MailboxService(db)

			if err := mailboxes.DeleteExpiredMailboxes(test.Logger()); (err != nil) != tt.wantErr {
				t.Errorf("deleteExpiredMailboxes() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.validateDB != nil {
				tt.validateDB(db)
			}
		})
	}
}

func TestDisableMailboxByExternalID(t *testing.T) {
	Assert := assert.New(t)

	type args struct {
		id string
	}

	tests := []struct {
		name      string
		args      args
		dbSetup   test.DBOption
		validater func(db *letterboxfakes.FakeDatastore)
		wantErr   bool
	}{
		{
			name: "Disables a mailbox by a given external ID",
			args: args{
				id: "abcdefg",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByExternalIDReturns(test.Mailbox(), nil)
			},
			validater: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(1, db.UpdateMailboxCallCount())
				m := db.UpdateMailboxArgsForCall(0)
				Assert.False(m.Enabled)
			},
			wantErr: false,
		},
		{
			name: "Errors if the specified mailbox can't be found",
			args: args{
				id: "abcdefg",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByExternalIDReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
		{
			name: "Errors if there's an error during update",
			args: args{
				id: "abcdefg",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByExternalIDReturns(test.Mailbox(), nil)
				db.UpdateMailboxReturns(ErrBackendError)
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := test.DB(tt.dbSetup)
			mailboxes := test.MailboxService(db)
			if err := mailboxes.DisableMailboxByExternalID(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DisableMailboxByExternalID() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.validater != nil {
				tt.validater(db)
			}
		})
	}
}

func TestGetOrCreateMailboxForAddress(t *testing.T) {
	type args struct {
		to string
	}

	tests := []struct {
		name       string
		args       args
		dbSetup    test.DBOption
		dbValidate func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions)
		wantErr    bool
	}{
		{
			name: "returns a mailbox that already exists",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(&Domain{Domain: "example.com", CatchallEnabled: true}, nil)
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				assert.Equal(0, db.UpdateMailboxCallCount())
			},
		},
		{
			name: "creates a mailbox if catchall is enabled and mailbox doesn't exist",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(&Domain{
					Domain:          "example.com",
					CatchallEnabled: true,
				}, nil)
				db.GetMailboxByDomainReturnsOnCall(0, nil, ErrNotFound)
				db.GetMailboxByDomainReturnsOnCall(1, &Mailbox{}, nil)
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore, assert *assert.Assertions) {
				assert.Equal(1, db.UpdateMailboxCallCount())
			},
		},
		{
			name:    "errors if address is invalid",
			args:    args{to: "foo@example.com@foo"},
			wantErr: true,
		},
		{
			name: "errors if domain doesn't exist",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
		{
			name: "errors if mailbox doesn't exist and domain doesn't allow catchalls",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(&Domain{
					Domain:          "example.com",
					CatchallEnabled: false,
				}, nil)
				db.GetMailboxByDomainReturns(nil, ErrNotFound)
			},
			wantErr: true,
		},
		{
			name: "errors if mailbox update fails",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(&Domain{
					Domain:          "example.com",
					CatchallEnabled: true,
				}, nil)
				db.GetMailboxByDomainReturns(nil, ErrNotFound)
				db.UpdateMailboxReturns(ErrBackendError)
			},
			wantErr: true,
		},
		{
			name: "errors if retrieval of the new mailbox fails",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(&Domain{
					Domain:          "example.com",
					CatchallEnabled: true,
				}, nil)
				db.GetMailboxByDomainReturnsOnCall(0, nil, ErrNotFound)
				db.GetMailboxByDomainReturnsOnCall(1, nil, ErrBackendError)
			},
			wantErr: true,
		},
		{
			name: "errors if mailbox retrieval fails from anything other than 'not found'",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(nil, ErrBackendError)
			},
			wantErr: true,
		},
		{
			name: "errors if retrieved mailbox is disabled",
			args: args{to: "foo@example.com"},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxByDomainReturns(&Mailbox{Enabled: false}, nil)
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert := assert.New(t)

			db := test.DB(tt.dbSetup)
			mailboxes := test.MailboxService(db)
			_, err := mailboxes.GetOrCreateMailboxForAddress(tt.args.to)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetOrCreateMailboxForAddress() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.dbValidate != nil {
				tt.dbValidate(db, assert)
			}
		})
	}
}
