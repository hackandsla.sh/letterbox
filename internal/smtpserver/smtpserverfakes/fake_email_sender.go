// Code generated by counterfeiter. DO NOT EDIT.
package smtpserverfakes

import (
	"sync"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver"
)

type FakeEmailSender struct {
	SendMailStub        func(*letterbox.ForwardingMessage) error
	sendMailMutex       sync.RWMutex
	sendMailArgsForCall []struct {
		arg1 *letterbox.ForwardingMessage
	}
	sendMailReturns struct {
		result1 error
	}
	sendMailReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeEmailSender) SendMail(arg1 *letterbox.ForwardingMessage) error {
	fake.sendMailMutex.Lock()
	ret, specificReturn := fake.sendMailReturnsOnCall[len(fake.sendMailArgsForCall)]
	fake.sendMailArgsForCall = append(fake.sendMailArgsForCall, struct {
		arg1 *letterbox.ForwardingMessage
	}{arg1})
	stub := fake.SendMailStub
	fakeReturns := fake.sendMailReturns
	fake.recordInvocation("SendMail", []interface{}{arg1})
	fake.sendMailMutex.Unlock()
	if stub != nil {
		return stub(arg1)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeEmailSender) SendMailCallCount() int {
	fake.sendMailMutex.RLock()
	defer fake.sendMailMutex.RUnlock()
	return len(fake.sendMailArgsForCall)
}

func (fake *FakeEmailSender) SendMailCalls(stub func(*letterbox.ForwardingMessage) error) {
	fake.sendMailMutex.Lock()
	defer fake.sendMailMutex.Unlock()
	fake.SendMailStub = stub
}

func (fake *FakeEmailSender) SendMailArgsForCall(i int) *letterbox.ForwardingMessage {
	fake.sendMailMutex.RLock()
	defer fake.sendMailMutex.RUnlock()
	argsForCall := fake.sendMailArgsForCall[i]
	return argsForCall.arg1
}

func (fake *FakeEmailSender) SendMailReturns(result1 error) {
	fake.sendMailMutex.Lock()
	defer fake.sendMailMutex.Unlock()
	fake.SendMailStub = nil
	fake.sendMailReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeEmailSender) SendMailReturnsOnCall(i int, result1 error) {
	fake.sendMailMutex.Lock()
	defer fake.sendMailMutex.Unlock()
	fake.SendMailStub = nil
	if fake.sendMailReturnsOnCall == nil {
		fake.sendMailReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.sendMailReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeEmailSender) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.sendMailMutex.RLock()
	defer fake.sendMailMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeEmailSender) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ smtpserver.EmailSender = new(FakeEmailSender)
