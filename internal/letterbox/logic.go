package letterbox

import (
	"fmt"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/ProtonMail/gopenpgp/v2/helper"
)

// UpdateUserKey updates the user key for the given user.
func UpdateUserKey(user *User, newKey *UserKey, password string, db Datastore) error {
	key, err := db.GetUserKey(user)
	if err != nil {
		return err
	}

	oldKey := &UserKey{
		PrivateKey: key.PrivateKey,
		PublicKey:  key.PublicKey,
	}

	key.PrivateKey = ""
	key.PublicKey = newKey.PublicKey

	// Validate that the public key is a valid armored key.
	if _, err := crypto.NewKeyFromArmored(key.PublicKey); err != nil {
		return err
	}

	if err := db.UpdateUserKey(key); err != nil {
		return fmt.Errorf("%w: couldn't update user key for user '%s': %v", ErrBackendError, user.Username, err)
	}

	// Rotate all the messages to be encrypted under the new key.
	if err := RotateMessageKeys(user, password, oldKey, key, db); err != nil {
		return fmt.Errorf("%w: couldn't rotate message key for user '%s': %v", ErrBackendError, user.Username, err)
	}

	return nil
}

// RotateMessageKeys rotates the keys used to encrypt all messages owned by a
// particular user. It attempts to re-encrypt every message it can; however, if
// there is a message encrypted with an unknown key, it will be left alone.
func RotateMessageKeys(user *User, password string, oldKey, newKey *UserKey, db Datastore) error {
	messages, err := db.GetMessagesForUser(user)
	if err != nil {
		return err
	}

	oldFingerprints, err := helper.GetSHA256Fingerprints(oldKey.PublicKey)
	if err != nil {
		return err
	}

	var reencrypted []EncryptedMessage //nolint:prealloc // It's unknown what the final size is

	for _, message := range messages {
		// Validate whether the message should be decryptable with the old key
		if message.Fingerprint != oldFingerprints[0] {
			continue
		}

		var unencrypted *Message

		err := DecryptObject(oldKey, password, message.Content, &unencrypted)
		if err != nil {
			return err
		}

		encrypted, err := EncryptMessage(unencrypted, newKey)
		if err != nil {
			return err
		}

		message.Content = encrypted.Content
		message.Fingerprint = encrypted.Fingerprint
		reencrypted = append(reencrypted, message)
	}

	for i := 0; i < len(reencrypted); i++ {
		if err := db.UpdateMessage(&reencrypted[i]); err != nil {
			return err
		}
	}

	return nil
}
