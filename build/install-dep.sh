#!/bin/bash

# This script is to install all required binaries into the goreleaser container
# during build

# Install Alpine APKs
apk add --update nodejs nodejs-npm make git-lfs build-base

# Install NPM scripts
npm config set user 0
npm config set unsafe-perm true
npm install -g redoc-cli