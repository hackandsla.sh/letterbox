package db

import "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"

func (db *SQLDatastore) AddUserKey(user *letterbox.User, key *letterbox.UserKey) error {
	key.UserID = user.ID
	return db.DB.Create(key).Error
}

func (db *SQLDatastore) GetUserKey(user *letterbox.User) (*letterbox.UserKey, error) {
	q := &letterbox.UserKey{
		UserID: user.ID,
	}
	key := &letterbox.UserKey{}

	if err := db.DB.Where(q).First(key).Error; err != nil {
		return nil, err
	}

	return key, nil
}

func (db *SQLDatastore) UpdateUserKey(key *letterbox.UserKey) error {
	return db.DB.Save(key).Error
}
