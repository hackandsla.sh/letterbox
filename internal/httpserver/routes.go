package httpserver

import (
	"github.com/markbates/pkger"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (s *HTTPService) initRoutes(root *SubRouter) {
	root.RouteFiles("/doc", pkger.Dir("/static/doc"))
	root.Route("GET", "/healthz", s.handleHealthz())
	root.Route("POST", "/init", s.handleAddFirstUser())

	root.Route("GET", "/disable/:id", s.handleDisableMailbox())

	v1 := root.SubRoute("/api/v1")
	v1.Use(authMiddleware(s))
	v1.Route("GET", "/", s.handleHello())
	v1.Handler("GET", "/metrics", promhttp.Handler())

	v1.Route("POST", "/users", s.handleAddOneUser())
	v1.Route("GET", "/users", s.handleGetAllUsers())
	v1.Route("GET", "/users/:username", s.handleGetOneUser())
	v1.Route("POST", "/users/:username", s.handleSetOneUser())
	v1.Route("POST", "/users/:username/key", s.handleSetOneUserKey())

	v1.Route("POST", "/domains", s.handleAddOneDomain())
	v1.Route("GET", "/domains", s.handleGetAllDomains())
	v1.Route("GET", "/domains/:domain", s.handleGetOneDomain())
	v1.Route("DELETE", "/domains/:domain", s.handleDeleteOneDomain())
	v1.Route("GET", "/domains/:domain/mailboxes", s.handleGetAllMailboxes())
	v1.Route("GET", "/domains/:domain/mailboxes/:name", s.handleGetOneMailbox())
	v1.Route("PUT", "/domains/:domain/mailboxes/:name", s.handleUpsertMailbox())
	v1.Route("PATCH", "/domains/:domain/mailboxes/:name", s.handlePatchMailbox())
	v1.Route("DELETE", "/domains/:domain/mailboxes/:name", s.handleDeleteOneMailbox())
	v1.Route("GET", "/domains/:domain/mailboxes/:name/messages", s.handleGetMessages())
	v1.Route("GET", "/domains/:domain/mailboxes/:name/stats", s.handleGetMailboxStats())
}
