package letterbox

import (
	"errors"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/jinzhu/gorm"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

// Domain represents an RFC1034 domain name.
type Domain struct {
	gorm.Model      `json:"-"`
	Domain          string `gorm:"type:varchar(255)" json:"domain,omitempty"`
	CatchallEnabled bool   `json:"catchall_enabled"`
	UserID          uint   `json:"-"`
}

func newDomain(params *AddDomainParams, userID uint) *Domain {
	return &Domain{
		Domain:          params.Domain,
		CatchallEnabled: getBoolPOrDefault(params.CatchallEnabled, true),
		UserID:          userID,
	}
}

// DomainStore represents the persistence and retrieval mechanisms for
// Domain objects.
type DomainStore interface {
	AddDomain(d *Domain) error
	GetDomain(name string) (*Domain, error)
	GetAllDomains() ([]Domain, error)
	DeleteDomain(d *Domain) error
}

type AddDomainParams struct {
	Domain          string `json:"domain,omitempty"`
	CatchallEnabled *bool  `json:"catchall_enabled"`
}

func (d *AddDomainParams) Validate() error {
	return wrapErr(
		ErrValidationError,
		validation.ValidateStruct(d,
			validation.Field(&d.Domain, validation.Required, is.Domain),
		),
	)
}

type DomainService struct {
	log logger.Logger
	db  DomainStore
}

func NewDomainService(log logger.Logger, db DomainStore) *DomainService {
	return &DomainService{
		log: log,
		db:  db,
	}
}

func (ds *DomainService) AddDomain(params *AddDomainParams, userID uint) error {
	log := ds.log.With(logger.Fields{
		"domain": params.Domain,
		"user":   userID,
	})

	if err := params.Validate(); err != nil {
		return err
	}

	exists, err := ds.doesDomainExist(params.Domain)
	if err != nil {
		log.Errorw("error lookup up domain", err)
		return err
	}

	if exists {
		return fmt.Errorf("error adding domain '%s': %w", params.Domain, ErrAlreadyExists)
	}

	domain := newDomain(params, userID)

	if err := ds.db.AddDomain(domain); err != nil {
		log.Errorw("error saving domain", err)
		return fmt.Errorf("error adding domain '%s': %w", domain.Domain, err)
	}

	return nil
}

func (ds *DomainService) doesDomainExist(domain string) (bool, error) {
	_, err := ds.GetDomain(domain)
	if errors.Is(err, ErrNotFound) {
		return false, nil
	}

	if err != nil {
		return false, err
	}

	return true, nil
}

// GetDomain retreives the domain object for a specific domain name.
func (ds *DomainService) GetDomain(domain string) (*Domain, error) {
	d, err := ds.db.GetDomain(domain)
	if err != nil {
		return nil, fmt.Errorf("could not get domain '%s': %w", domain, err)
	}

	return d, nil
}

// DeleteDomain deletes the domain with the given name.
func (ds *DomainService) DeleteDomain(domain string) error {
	d, err := ds.GetDomain(domain)
	if err != nil {
		return err
	}

	return ds.db.DeleteDomain(d)
}
