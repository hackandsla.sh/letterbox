package server

import (
	"fmt"
	stdlog "log"
	"os"
	"os/signal"
	"syscall"

	"github.com/alexedwards/argon2id"
	_ "github.com/jinzhu/gorm/dialects/postgres" // Use Postgres GORM dialect

	"gitlab.com/hackandsla.sh/letterbox/internal/badactorservice"
	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/cleanup"
	"gitlab.com/hackandsla.sh/letterbox/internal/db"
	"gitlab.com/hackandsla.sh/letterbox/internal/httpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger/zap"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpclient"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/dkim"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/spf"
)

// Server describes the server configuration.
type Server struct {
	Config             Config
	Info               *letterbox.Info
	DB                 letterbox.Datastore
	HTTP               *httpserver.HTTPService
	SMTP               *smtpserver.SMTPService
	Cleanup            *cleanup.Cleanup
	RegisteredServices []Service
	Hasher             letterbox.ArgonHasher
	Banner             *banner.Builder
}

func New(config *Config, i *letterbox.Info) *Server {
	return &Server{
		Config: *config,
		Info:   i,
	}
}

// Start initializes a server instance and start the server.
//nolint:funlen // This is just startup wiring code
func (srv *Server) Start() error {
	var err error

	srv.Hasher = letterbox.ArgonHasher{
		Params: *argon2id.DefaultParams,
	}

	srv.Banner, err = banner.NewBuilder(srv.Config.Banner)
	if err != nil {
		return fmt.Errorf("couldn't initialize banner builder: %w", err)
	}

	log, err := zap.New(srv.Config.Log)
	if err != nil {
		stdlog.Printf("could not initialize logger service: %s", err)
		return nil
	}

	srv.RegisterService(log)

	sqlDB := db.New(srv.Config.DB)
	srv.DB = sqlDB
	srv.RegisterService(sqlDB)

	badActorsStudio, err := badactorservice.New(srv.Config.BadActor, log)
	if err != nil {
		return fmt.Errorf("error while initializing bad actors service: %w", err)
	}

	domains := letterbox.NewDomainService(log, sqlDB)

	mailboxes := letterbox.NewMailboxService(srv.DB, domains)

	messages := letterbox.NewMessageService(log, sqlDB, sqlDB, sqlDB)

	users := letterbox.NewUserService(srv.Hasher, sqlDB, sqlDB)

	stats := letterbox.NewMailboxStatsService(sqlDB)

	srv.HTTP = httpserver.New(
		srv.Config.HTTP,
		srv.DB,
		log,
		srv.Info,
		srv.Hasher,
		badActorsStudio,
		domains,
		mailboxes,
		messages,
		users,
		sqlDB,
	)
	srv.RegisterService(srv.HTTP)

	domainAlignmentCheck := &letterbox.FlexibleDomainCheck{}

	dkimCheck := &dkim.Client{
		DomainAlignment: domainAlignmentCheck,
	}

	spfCheck := &spf.Client{
		DomainAlignment: domainAlignmentCheck,
	}

	var smtpClient smtpserver.EmailSender
	if srv.Config.SMTP.Outbound.Host != "" {
		smtpClient, err = smtpclient.New(srv.Config.SMTP.Outbound, log)
		if err != nil {
			return fmt.Errorf("error while initializing SMTP client: %w", err)
		}
	} else {
		log.Infow("no outbound SMTP server specified; skipping client configuration")
		smtpClient = smtpclient.NewNopClient(log)
	}

	srv.SMTP = smtpserver.New(
		*srv.Config.SMTP,
		srv.DB,
		log,
		srv.Banner,
		dkimCheck,
		spfCheck,
		smtpClient,
		mailboxes,
		messages,
		stats,
	)

	srv.RegisterService(srv.SMTP)

	srv.Cleanup = cleanup.New(srv.Config.Cleanup, log, mailboxes)
	srv.RegisterService(srv.Cleanup)

	for _, service := range srv.RegisteredServices {
		if err := service.Start(); err != nil {
			log.Errorf("could not start service '%s': %v", service.Name(), err)
			return nil
		}

		log.Infow("started service",
			logger.Fields{
				"service": service.Name(),
			},
		)
	}

	initConfig := srv.Config.Init
	if initConfig.Username != "" && initConfig.Password != "" {
		// TODO while we expect most of these errors should be ignored, it would
		// be nice to differentiate certain errors (e.g. DB errors) from
		// security ones.
		_ = users.AddInitialAdminUser(
			&letterbox.AddUserParams{
				Username: initConfig.Username,
				Password: initConfig.Password,
			})
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	log.Infow("beginning shutdown")

	// Shutdown services in reverse order so their dependency chains stay valid
	for i := len(srv.RegisteredServices) - 1; i >= 0; i-- {
		service := srv.RegisteredServices[i]
		if err := service.Stop(); err != nil {
			log.Errorf("could not stop service %s: %s\n", service.Name(), err.Error())
		}

		log.Infow("stopped service",
			logger.Fields{
				"service": service.Name(),
			},
		)
	}
	log.Info("server stopped")

	return nil
}

func (srv *Server) RegisterService(service Service) {
	srv.RegisteredServices = append(srv.RegisteredServices, service)
}

type Service interface {
	Name() string
	Start() error
	Stop() error
}

type LoggerService interface {
	Service
	logger.Logger
}
