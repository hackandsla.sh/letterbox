package httpserver

import (
	"fmt"
	"net/http"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func (s *HTTPService) handleAddOneUser() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		var userParams letterbox.AddUserParams
		if err := s.decodeJSONBody(w, r, &userParams); err != nil {
			return nil, err
		}

		if err := s.users.AddUser(&userParams); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleAddFirstUser() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		var userParams letterbox.AddUserParams
		if err := s.decodeJSONBody(w, r, &userParams); err != nil {
			return nil, err
		}

		if err := s.users.AddInitialAdminUser(&userParams); err != nil {
			return nil, ErrUnauthorized
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleGetOneUser() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")

		return s.users.GetUserScrubbed(paramUsername)
	}
}

func (s *HTTPService) handleSetOneUser() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")

		body := &letterbox.User{}
		if err := s.decodeJSONBody(w, r, &body); err != nil {
			return nil, err
		}

		authenticatedUser, err := getAuthenticatedUser(r)
		if err != nil {
			return nil, ErrUnauthorized
		}

		_, password, authOK := r.BasicAuth()
		if !authOK {
			return nil, ErrUnauthorized
		}

		// TODO move this to a centralized RBAC location
		if authenticatedUser.Username != paramUsername {
			return nil, fmt.Errorf("%w: a user may only edit their own account", ErrUnauthorized)
		}

		if err := s.users.UpdateUser(paramUsername, password, body); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleSetOneUserKey() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")
		// TODO use a different model for inputs, rather than using our internal model
		body := &letterbox.UserKey{}
		if err := s.decodeJSONBody(w, r, &body); err != nil {
			return nil, err
		}

		authenticatedUser, err := getAuthenticatedUser(r)
		if err != nil {
			return nil, ErrUnauthorized
		}

		_, password, authOK := r.BasicAuth()
		if !authOK {
			return nil, ErrUnauthorized
		}

		// TODO move this to a centralized RBAC location
		if authenticatedUser.Username != paramUsername {
			return nil, fmt.Errorf("%w: a user may only edit their own account", ErrUnauthorized)
		}

		if err := letterbox.UpdateUserKey(authenticatedUser, body, password, s.db); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleGetAllUsers() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		return s.users.GetAllUsersScrubbed()
	}
}
