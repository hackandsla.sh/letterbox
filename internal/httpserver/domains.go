package httpserver

import (
	"fmt"
	"net/http"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func (s *HTTPService) handleAddOneDomain() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		user, err := getAuthenticatedUser(r)
		if err != nil {
			return nil, err
		}

		params := &letterbox.AddDomainParams{}
		if err := s.decodeJSONBody(w, r, &params); err != nil {
			return nil, err
		}

		if err := s.domains.AddDomain(params, user.ID); err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleGetOneDomain() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")

		domain, err := s.domains.GetDomain(paramDomain)
		if err != nil {
			return nil, err
		}

		return domain, nil
	}
}

func (s *HTTPService) handleDeleteOneDomain() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")

		err := s.domains.DeleteDomain(paramDomain)
		if err != nil {
			return nil, err
		}

		return successResponse(), nil
	}
}

func (s *HTTPService) handleGetAllDomains() HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		domains, err := s.db.GetAllDomains()
		if err != nil {
			return nil, fmt.Errorf("could not get domains: %w", err)
		}

		return domains, nil
	}
}
