package db

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func (db *SQLDatastore) GetMailboxesByDomain(domain *letterbox.Domain) ([]letterbox.Mailbox, error) {
	m := []letterbox.Mailbox{}
	q := &letterbox.Mailbox{
		DomainID: domain.ID,
	}

	if err := db.DB.Find(&m, &q).Error; err != nil {
		return nil, err
	}

	return m, nil
}

func (db *SQLDatastore) GetMailboxByDomain(domain *letterbox.Domain, name string) (*letterbox.Mailbox, error) {
	q := &letterbox.Mailbox{
		Name:     name,
		DomainID: domain.ID,
	}

	u := &letterbox.Mailbox{}
	if err := db.DB.Find(u, q).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, fmt.Errorf("can't find mailbox with name %s: %w", name, letterbox.ErrNotFound)
		}

		return nil, fmt.Errorf("can't find mailbox with name %s: %w: %v", name, letterbox.ErrBackendError, err)
	}

	return u, nil
}

func (db *SQLDatastore) GetAllMailboxes() ([]letterbox.Mailbox, error) {
	m := []letterbox.Mailbox{}
	if err := db.DB.Find(&m).Error; err != nil {
		return nil, err
	}

	return m, nil
}

func (db *SQLDatastore) UpdateMailbox(m *letterbox.Mailbox) error {
	if err := db.DB.Save(m).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) DeleteMailbox(m *letterbox.Mailbox) error {
	if m.ID == 0 {
		return letterbox.ErrMissingID
	}

	return db.DB.Unscoped().Delete(m).Error
}

func (db *SQLDatastore) GetExpiredMailboxes() ([]letterbox.Mailbox, error) {
	ret := []letterbox.Mailbox{}
	now := time.Now()

	if err := db.DB.Where("expires < ?", now).Find(&ret).Error; err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *SQLDatastore) GetMailboxByExternalID(id string) (*letterbox.Mailbox, error) {
	m := &letterbox.Mailbox{}
	if err := db.DB.Where(&letterbox.Mailbox{ExternalID: id}).First(m).Error; err != nil {
		return nil, fmt.Errorf("couldn't find mailbox with external ID '%s': %w", id, wrapGormErr(err))
	}

	return m, nil
}
