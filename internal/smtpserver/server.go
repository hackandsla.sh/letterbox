package smtpserver

import (
	"crypto/tls"
	"fmt"
	"time"

	"github.com/docker/go-units"
	smtp "github.com/emersion/go-smtp"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpclient"
	"gitlab.com/hackandsla.sh/letterbox/internal/validate"
)

const tcpMax int = 65535

type Config struct {
	Hostname string             `json:"hostname,omitempty"`
	Port     int                `json:"port,omitempty"`
	MaxSize  string             `json:"max_size,omitempty"`
	Outbound *smtpclient.Config `json:"outbound,omitempty"`
	TLS      *TLSConfig         `json:"tls,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Hostname, validation.Required, is.Host),
		validation.Field(&c.Port, validation.Required, validation.Min(1), validation.Max(tcpMax)),
		validation.Field(&c.MaxSize, validation.Required, validation.By(validate.IsBytesSize)),
		validation.Field(&c.Outbound),
		validation.Field(&c.TLS),
	)
}

type TLSConfig struct {
	// The certificate string
	Cert string `json:"cert,omitempty"`

	// The (unencrypted) private key string
	Key string `json:"key,omitempty"`

	// The path to the file containing the certificate
	CertFile string `json:"cert_file,omitempty"`

	// The path to the file containing the (unencrypted) private key
	KeyFile string `json:"key_file,omitempty"`

	// The port to use for the TLS-only SMTPS server
	// This server is wrapped with TLS, and doesn't use the STARTTLS function
	// See RFC8314
	Port int `json:"port,omitempty"`
}

func (c *TLSConfig) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.CertFile, validation.By(validate.FileExists)),
		validation.Field(&c.KeyFile, validation.By(validate.FileExists)),
	)
}

type SMTPService struct {
	config     Config
	server     *smtp.Server
	tlsServer  *smtp.Server
	smtpClient EmailSender
	maxSize    int64
	mailboxes  *letterbox.MailboxService
	messages   *letterbox.MessageService
	stats      *letterbox.MailboxStatsService

	// Service Dependencies
	db     letterbox.Datastore
	log    logger.Logger
	banner *banner.Builder
	dkim   dkimChecker
	spf    spfChecker
}

func New(
	cfg Config,
	db letterbox.Datastore,
	log logger.Logger,
	bannerBuilder *banner.Builder,
	dkim dkimChecker,
	spf spfChecker,
	client EmailSender,
	mailboxes *letterbox.MailboxService,
	messages *letterbox.MessageService,
	stats *letterbox.MailboxStatsService,
) *SMTPService {
	return &SMTPService{
		config:     cfg,
		db:         db,
		log:        log,
		banner:     bannerBuilder,
		dkim:       dkim,
		spf:        spf,
		smtpClient: client,
		mailboxes:  mailboxes,
		messages:   messages,
		stats:      stats,
	}
}

func (ss *SMTPService) Name() string {
	return "SMTP"
}

func (ss *SMTPService) Start() error {
	maxSize, err := units.FromHumanSize(ss.config.MaxSize)
	if err != nil {
		return err
	}

	ss.maxSize = maxSize

	tlsConfig, err := GetTLSConfig(ss.config.TLS)
	if err != nil {
		return err
	} else if tlsConfig == nil {
		ss.log.Infow("no TLS configuration specified; skipping TLS server creation")
	}

	ss.server = NewBackend(ss, tlsConfig, ss.config.Port, int(maxSize), ss.config.Hostname)

	if tlsConfig != nil {
		ss.tlsServer = NewBackend(ss, tlsConfig, ss.config.TLS.Port, int(maxSize), ss.config.Hostname)
	}

	// TODO check for errors on startup
	if ss.server != nil {
		ss.log.Infow("starting SMTP server",
			logger.Fields{
				"address": ss.server.Addr,
			},
		)

		go func() {
			if err := ss.server.ListenAndServe(); err != nil {
				ss.log.InfoErr("error while starting up HTTP server", err)
			}
		}()
	}

	if ss.tlsServer != nil {
		ss.log.Infow("starting TLS-only SMTP server",
			logger.Fields{
				"address": ss.tlsServer.Addr,
			},
		)

		go func() {
			if err := ss.tlsServer.ListenAndServeTLS(); err != nil {
				ss.log.InfoErr("error while starting up HTTPS servers", err)
			}
		}()
	}

	ss.log.Infow("SMTP service started")

	return nil
}

func NewBackend(ss smtp.Backend, tc *tls.Config, port, maxSize int, hostname string) *smtp.Server {
	smtpServer := smtp.NewServer(ss)

	smtpServer.TLSConfig = tc
	smtpServer.Addr = fmt.Sprintf(":%d", port)
	smtpServer.Domain = hostname
	smtpServer.MaxMessageBytes = maxSize

	// TODO expose these as higher-level variables
	smtpServer.ReadTimeout = 100 * time.Second
	smtpServer.WriteTimeout = 100 * time.Second
	smtpServer.MaxRecipients = 50
	smtpServer.AuthDisabled = true

	return smtpServer
}

func (ss *SMTPService) Stop() error {
	if ss.server != nil {
		ss.server.Close()
	}

	if ss.tlsServer != nil {
		ss.tlsServer.Close()
	}

	return nil
}

func GetTLSConfig(c *TLSConfig) (*tls.Config, error) {
	var certificate tls.Certificate

	var err error

	if c.Cert != "" && c.Key != "" {
		certificate, err = tls.X509KeyPair([]byte(c.Cert), []byte(c.Key))
	} else if c.CertFile != "" && c.KeyFile != "" {
		certificate, err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
	} else {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &tls.Config{
		Certificates: []tls.Certificate{
			certificate,
		},
		MinVersion: tls.VersionTLS12,
	}, nil
}
