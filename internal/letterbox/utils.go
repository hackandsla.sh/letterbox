package letterbox

import (
	"errors"
	"time"
)

func getBoolPOrDefault(b *bool, defaultValue bool) bool {
	if b == nil {
		return defaultValue
	}

	return *b
}

func getTimePOrDefault(t, defaultValue *time.Time) *time.Time {
	if t == nil {
		return defaultValue
	}

	rounded := t.Round(time.Second)

	return &rounded
}

func getStringSliceOrDefault(ss, defaultValue []string) []string {
	if ss == nil {
		return defaultValue
	}

	return ss
}

func itemNotFound(err error) bool {
	return errors.Is(err, ErrNotFound)
}
