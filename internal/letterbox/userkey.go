package letterbox

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/jinzhu/gorm"
)

// UserKey defines a GPG key stored in our server. If this contains a private
// key, it will be used for decryption as well as encryption.
type UserKey struct {
	gorm.Model `json:"-"`
	UserID     uint   `json:"-"`
	PrivateKey string `json:"-"`
	PublicKey  string `json:"public_key,omitempty"`
}

// Validate validates that the given UserKey is valid.
// TODO validate that the contents look like public/private keys.
func (u *UserKey) Validate() error {
	return validation.ValidateStruct(u,
		validation.Field(&u.UserID, validation.Required),
		validation.Field(&u.PrivateKey, validation.Required),
		validation.Field(&u.PublicKey, validation.Required),
	)
}

// UserKeyStore defines our persistence and retrieval mechanisms for
// UserKeys.
type UserKeyStore interface {
	AddUserKey(user *User, key *UserKey) error
	GetUserKey(user *User) (*UserKey, error)
	UpdateUserKey(key *UserKey) error
}
