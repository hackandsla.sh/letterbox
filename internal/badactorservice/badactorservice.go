package badactorservice

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/jaredfolkins/badactor"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

const (
	maxStudios   int32         = 1024
	maxDirectors int32         = 1024
	pollDuration time.Duration = 60 * time.Second
)

type Config struct {
	MaxAuthAttempts int           `json:"max_auth_attempts,omitempty"`
	JailTime        time.Duration `json:"jail_time,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.MaxAuthAttempts, validation.Required, validation.Min(1)),
		validation.Field(&c.JailTime, validation.Required),
	)
}

func New(config *Config, log logger.Logger) (*badactor.Studio, error) {
	studio := badactor.NewStudio(maxStudios)
	rule := &badactor.Rule{
		Name:        "Login",
		Message:     "You have failed to login too many times",
		StrikeLimit: config.MaxAuthAttempts,
		ExpireBase:  config.JailTime,
		Sentence:    config.JailTime,
		Action: &badActorAction{
			log: log,
		},
	}
	studio.AddRule(rule)

	if err := studio.CreateDirectors(maxDirectors); err != nil {
		return nil, err
	}

	studio.StartReaper(pollDuration)

	return studio, nil
}

type badActorAction struct {
	log logger.Logger
}

func (b *badActorAction) WhenJailed(a *badactor.Actor, r *badactor.Rule) error {
	b.log.Infow("bad actor banned",
		logger.Fields{
			"rule":    r.Name,
			"message": r.Message,
			// "ip", a.Name // TODO This depends on https://github.com/jaredfolkins/badactor/pull/7
		},
	)

	return nil
}

func (b *badActorAction) WhenTimeServed(a *badactor.Actor, r *badactor.Rule) error {
	b.log.Infow("bad actor unbanned",
		logger.Fields{
			"rule": r.Name,
			// "ip", a.Name // TODO This depends on https://github.com/jaredfolkins/badactor/pull/7
		},
	)

	return nil
}
