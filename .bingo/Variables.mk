# Auto generated binary variables helper managed by https://github.com/bwplotka/bingo v0.4.0. DO NOT EDIT.
# All tools are designed to be build inside $GOBIN.
BINGO_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
GOPATH ?= $(shell go env GOPATH)
GOBIN  ?= $(firstword $(subst :, ,${GOPATH}))/bin
GO     ?= $(shell which go)

# Below generated variables ensure that every time a tool under each variable is invoked, the correct version
# will be used; reinstalling only if needed.
# For example for bingo variable:
#
# In your main Makefile (for non array binaries):
#
#include .bingo/Variables.mk # Assuming -dir was set to .bingo .
#
#command: $(BINGO)
#	@echo "Running bingo"
#	@$(BINGO) <flags/args..>
#
BINGO := $(GOBIN)/bingo-v0.4.0
$(BINGO): $(BINGO_DIR)/bingo.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/bingo-v0.4.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=bingo.mod -o=$(GOBIN)/bingo-v0.4.0 "github.com/bwplotka/bingo"

CFSSL := $(GOBIN)/cfssl-v1.5.0
$(CFSSL): $(BINGO_DIR)/cfssl.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/cfssl-v1.5.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=cfssl.mod -o=$(GOBIN)/cfssl-v1.5.0 "github.com/cloudflare/cfssl/cmd/cfssl"

COMPILEDAEMON := $(GOBIN)/compiledaemon-v1.2.1
$(COMPILEDAEMON): $(BINGO_DIR)/compiledaemon.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/compiledaemon-v1.2.1"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=compiledaemon.mod -o=$(GOBIN)/compiledaemon-v1.2.1 "github.com/githubnemo/CompileDaemon"

COUNTERFEITER := $(GOBIN)/counterfeiter-v6.3.0
$(COUNTERFEITER): $(BINGO_DIR)/counterfeiter.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/counterfeiter-v6.3.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=counterfeiter.mod -o=$(GOBIN)/counterfeiter-v6.3.0 "github.com/maxbrunsfeld/counterfeiter/v6"

GO_ENUM := $(GOBIN)/go-enum-v0.2.4
$(GO_ENUM): $(BINGO_DIR)/go-enum.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/go-enum-v0.2.4"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=go-enum.mod -o=$(GOBIN)/go-enum-v0.2.4 "github.com/abice/go-enum"

GO_JUNIT_REPORT := $(GOBIN)/go-junit-report-v0.9.1
$(GO_JUNIT_REPORT): $(BINGO_DIR)/go-junit-report.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/go-junit-report-v0.9.1"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=go-junit-report.mod -o=$(GOBIN)/go-junit-report-v0.9.1 "github.com/jstemmer/go-junit-report"

GOLANGCI_LINT := $(GOBIN)/golangci-lint-v1.32.1
$(GOLANGCI_LINT): $(BINGO_DIR)/golangci-lint.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/golangci-lint-v1.32.1"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=golangci-lint.mod -o=$(GOBIN)/golangci-lint-v1.32.1 "github.com/golangci/golangci-lint/cmd/golangci-lint"

GORELEASER := $(GOBIN)/goreleaser-v0.155.0
$(GORELEASER): $(BINGO_DIR)/goreleaser.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/goreleaser-v0.155.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=goreleaser.mod -o=$(GOBIN)/goreleaser-v0.155.0 "github.com/goreleaser/goreleaser"

LSIF_GO := $(GOBIN)/lsif-go-v1.2.0
$(LSIF_GO): $(BINGO_DIR)/lsif-go.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/lsif-go-v1.2.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=lsif-go.mod -o=$(GOBIN)/lsif-go-v1.2.0 "github.com/sourcegraph/lsif-go/cmd/lsif-go"

PKGER := $(GOBIN)/pkger-v0.17.1
$(PKGER): $(BINGO_DIR)/pkger.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/pkger-v0.17.1"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=pkger.mod -o=$(GOBIN)/pkger-v0.17.1 "github.com/markbates/pkger/cmd/pkger"

SWAGGER := $(GOBIN)/swagger-v0.25.0
$(SWAGGER): $(BINGO_DIR)/swagger.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/swagger-v0.25.0"
	@cd $(BINGO_DIR) && $(GO) build -mod=mod -modfile=swagger.mod -o=$(GOBIN)/swagger-v0.25.0 "github.com/go-swagger/go-swagger/cmd/swagger"

