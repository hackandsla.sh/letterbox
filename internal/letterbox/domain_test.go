package letterbox_test

import (
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestAddDomain(t *testing.T) {
	var userID uint = 1

	tests := []struct {
		name        string
		params      *letterbox.AddDomainParams
		dbSetup     test.DBOption
		dbValidate  func(t *testing.T, db *letterboxfakes.FakeDatastore)
		wantErrType error
	}{
		{
			name: "Should add a domain that doesn't exist",
			params: &letterbox.AddDomainParams{
				Domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, letterbox.ErrNotFound)
			},
			dbValidate: func(t *testing.T, db *letterboxfakes.FakeDatastore) {
				addedDomain := db.AddDomainArgsForCall(0)

				want := &letterbox.Domain{
					Domain:          "example.com",
					CatchallEnabled: true,
					UserID:          userID,
				}

				if err := deep.Equal(addedDomain, want); err != nil {
					t.Error(err)
				}
			},
			wantErrType: nil,
		},
		{
			name: "Errors if domain already exists",
			params: &letterbox.AddDomainParams{
				Domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(test.Domain(), nil)
			},
			wantErrType: letterbox.ErrAlreadyExists,
		},
		{
			name: "Errors if a backend error occurs while checking for domain",
			params: &letterbox.AddDomainParams{
				Domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, letterbox.ErrBackendError)
			},
			wantErrType: letterbox.ErrBackendError,
		},
		{
			name: "Errors if a backend error occurs while adding the domain",
			params: &letterbox.AddDomainParams{
				Domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetDomainReturns(nil, letterbox.ErrNotFound)
				db.AddDomainReturns(letterbox.ErrBackendError)
			},
			wantErrType: letterbox.ErrBackendError,
		},
		{
			name: "Errors if parameters are invalid",
			params: &letterbox.AddDomainParams{
				Domain: "example",
			},
			wantErrType: letterbox.ErrValidationError,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			db := test.DB(tt.dbSetup)
			log := test.Logger()
			domains := letterbox.NewDomainService(log, db)

			// Act
			err := domains.AddDomain(tt.params, userID)

			// Assert
			assert.ErrorIs(t, err, tt.wantErrType)

			if tt.dbValidate != nil {
				tt.dbValidate(t, db)
			}
		})
	}
}
