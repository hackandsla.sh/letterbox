package letterbox_test

import (
	"testing"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func TestStrictDomainCheck_ValidateDomainAlignment(t *testing.T) {
	type args struct {
		a string
		b string
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Should fail validation if domains aren't exactly equivalent",
			args: args{
				a: "example.com",
				b: "foo.example.com",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &letterbox.StrictDomainCheck{}
			if err := s.ValidateDomainAlignment(tt.args.a, tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("StrictDomainCheck.ValidateDomainAlignment() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFlexibleDomainCheck_ValidateDomainAlignment(t *testing.T) {
	type args struct {
		a string
		b string
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{

		{
			name: "Should validate two domains are equivalent",
			args: args{
				a: "example.com",
				b: "foo.example.com",
			},
			wantErr: false,
		},
		{
			name: "Should validate two domains given mixed case",
			args: args{
				a: "EXAMPLE.COM",
				b: "foo.example.com",
			},
			wantErr: false,
		},
		{
			name: "Should validate two domains from 2-level eTLDs",
			args: args{
				a: "example.co.uk",
				b: "foo.example.co.uk",
			},
			wantErr: false,
		},
		{
			name: "Should fail given 2 different organizational domains from 2-level eTLDs",
			args: args{
				a: "example.co.uk",
				b: "bar.co.uk",
			},
			wantErr: true,
		},

		{
			name: "Should fail given only a TLD",
			args: args{
				a: "example.com",
				b: "com",
			},
			wantErr: true,
		},
		{
			name: "Should fail given only a TLD 2",
			args: args{
				a: "co.uk",
				b: "example.co.uk",
			},
			wantErr: true,
		},
		{
			name: "Should fail validation if domains don't use the same organizational domain",
			args: args{
				a: "example.com",
				b: "asdf.com",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &letterbox.FlexibleDomainCheck{}
			if err := f.ValidateDomainAlignment(tt.args.a, tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("FlexibleDomainCheck.ValidateDomainAlignment() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
