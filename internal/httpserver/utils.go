package httpserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/docker/go-units"
	"github.com/golang/gddo/httputil/header"
	"github.com/julienschmidt/httprouter"
)

func respondJSON(w http.ResponseWriter, data interface{}, status int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if data != nil {
		if err := json.NewEncoder(w).Encode(data); err != nil {
			return err
		}
	}

	return nil
}

// decodeJSONBody attempts to perform a JSON deserialization of the given
// http.Request body. If an error occurs, a specific error message is returned
// to help the client determine what the issue was. See
// https://www.alexedwards.net/blog/how-to-properly-parse-a-json-request-body
func (s *HTTPService) decodeJSONBody(w http.ResponseWriter, r *http.Request, v interface{}) error {
	if r.Header.Get("Content-Type") != "" {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		if value != "application/json" {
			msg := "Content-Type header is not application/json"
			return &statusError{status: http.StatusUnsupportedMediaType, msg: msg}
		}
	}

	r.Body = http.MaxBytesReader(w, r.Body, s.maxBodyReadSize)

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(&v)
	if err != nil {
		var syntaxError *json.SyntaxError

		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)

			return &statusError{status: http.StatusBadRequest, msg: msg}

		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := "Request body contains badly-formed JSON"

			return &statusError{status: http.StatusBadRequest, msg: msg}

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf(
				"Request body contains an invalid value for the %q field (at position %d)",
				unmarshalTypeError.Field,
				unmarshalTypeError.Offset,
			)

			return &statusError{status: http.StatusBadRequest, msg: msg}

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)

			return &statusError{status: http.StatusBadRequest, msg: msg}

		case err.Error() == "http: request body too large":
			msg := fmt.Sprintf("Request body must not be larger than %s", units.BytesSize(float64(s.maxBodyReadSize)))

			return &statusError{status: http.StatusRequestEntityTooLarge, msg: msg}

		case errors.Is(err, io.EOF):
			// Allow empty bodies
			return nil

		default:
			return err
		}
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		msg := "Request body must only contain a single JSON object"
		return &statusError{status: http.StatusBadRequest, msg: msg}
	}

	return nil
}

func getVars(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}

func getVar(r *http.Request, param string) string {
	params := httprouter.ParamsFromContext(r.Context())
	return params.ByName(param)
}

func ReverseStringSlice(s []string) []string {
	lenArr := len(s)
	ret := make([]string, lenArr)

	for i := lenArr - 1; i >= 0; i-- {
		ret[lenArr-i-1] = s[i]
	}

	return ret
}
