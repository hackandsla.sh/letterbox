package letterbox_test

import (
	"flag"
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

var update = flag.Bool("update", false, "update the golden files")

func TestNewProxyMessage(t *testing.T) {
	type args struct {
		message *Message
	}

	tests := []struct {
		name    string
		mailbox *Mailbox
		args    args
		want    *ForwardingMessage
		wantErr bool
		dbSetup func(db *letterboxfakes.FakeDatastore)
	}{
		{
			name: "create a proxied email",
			mailbox: &Mailbox{
				Address:      "foo@example.com",
				ProxyAddress: "asdfzxcvqwerty@example.com",
				ExternalID:   "abcdefg",
			},
			args: args{
				message: &Message{
					FromHeader: "jim.jones@source.com",
					Subject:    "Hello World",
					TextBody:   "Hello",
					HTMLBody:   "<div>hello</div>",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetUserByIDReturns(&User{Email: "foo@target.com"}, nil)
			},
			want: &ForwardingMessage{
				FromHeader: "asdfzxcvqwerty@example.com",
				ToHeader:   "foo@target.com",
				Subject:    "[proxied] Hello World",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := test.DB(tt.dbSetup)

			bb := test.BannerBuilder(
				test.WithURL("http://foo.example.com"),
			)

			got, err := NewProxyMessage(tt.args.message, tt.mailbox, test.Logger(), db, bb)
			if (err != nil) != tt.wantErr {
				t.Errorf("SMTPSession.CreateProxyMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want.FromHeader, got.FromHeader)
			assert.Equal(t, tt.want.ToHeader, got.ToHeader)
			assert.Equal(t, tt.want.Subject, got.Subject)
			test.AssertMatchesGoldenFile(t, got.TextBody, tt.name, ".txt", update)
			test.AssertMatchesGoldenFile(t, got.HTMLBody, tt.name, ".html", update)
		})
	}
}
