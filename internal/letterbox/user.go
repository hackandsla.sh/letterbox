package letterbox

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/jinzhu/gorm"
)

// User stores information about a user.
type User struct {
	gorm.Model `json:"-"`
	Username   string `gorm:"unique_index;type:varchar(100)" json:"username,omitempty"`
	Email      string `gorm:"type:varchar(100)" json:"email,omitempty"`
	Password   string `gorm:"type:varchar(255)" json:"-"`
}

type AddUserParams struct {
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

// Validate validates whether the values of the User are valid.
func (u *AddUserParams) Validate() error {
	return validation.ValidateStruct(u,
		validation.Field(&u.Username, validation.Required, validation.Length(1, 100)),
		validation.Field(&u.Email, is.EmailFormat),
		validation.Field(&u.Password, validation.Required),
	)
}

// Scrubbed empties the password hash from a user object.
func (u User) Scrubbed() *User {
	u.Password = ""

	return &u
}

// UserStore defines all our persistence and retrieval mechanisms for a
// particular user.
type UserStore interface {
	AddUser(u *User) error
	GetUser(name string) (*User, error)
	UpdateUser(u *User) error
	GetAllUsers() ([]User, error)
}

type UserService struct {
	hasher ArgonHasher
	userDB UserStore
	keyDB  UserKeyStore
}

func NewUserService(hasher ArgonHasher, userDB UserStore, keyDB UserKeyStore) *UserService {
	return &UserService{
		hasher: hasher,
		userDB: userDB,
		keyDB:  keyDB,
	}
}

// AddInitialAdminUser checks (1) that there are no other users present, and (2)
// that the source request is from private IP space. If both are true, the given
// user is added as the initial admin account.
//
// TODO change this to use the HTTP API instead of interacting direcctly with the
// database.
func (us *UserService) AddInitialAdminUser(params *AddUserParams) error {
	if err := params.Validate(); err != nil {
		return fmt.Errorf("%w: %v", ErrValidationError, err)
	}

	// TODO add a limit here, since we just need to prove the existence of one user
	if err := us.CanAddInitialAdminUser(); err != nil {
		return err
	}

	if err := us.AddUser(params); err != nil {
		return err
	}

	return nil
}

// CanAddInitialAdminUser determines whether it's allowed to add the initial
// admin user. It is only allowed if there are no other users created.
func (us *UserService) CanAddInitialAdminUser() error {
	existingUsers, err := us.userDB.GetAllUsers()
	if err != nil {
		return err
	}

	if len(existingUsers) > 0 {
		return fmt.Errorf("error while adding admin user: %w", ErrAlreadyExists)
	}

	return nil
}

func (us *UserService) GetUserScrubbed(name string) (*User, error) {
	u, err := us.userDB.GetUser(name)
	if err != nil {
		return nil, fmt.Errorf("error getting user '%s': %w", name, ErrNotFound)
	}

	return u.Scrubbed(), nil
}

func (us *UserService) GetAllUsersScrubbed() ([]*User, error) {
	users, err := us.userDB.GetAllUsers()
	if err != nil {
		return nil, fmt.Errorf("error getting users: %w", err)
	}

	var scrubbed []*User
	for _, user := range users {
		scrubbed = append(scrubbed, user.Scrubbed())
	}

	return scrubbed, nil
}

// AddUser adds a user to the datastore.
func (us *UserService) AddUser(params *AddUserParams) error {
	if err := params.Validate(); err != nil {
		return err
	}

	hashed, err := us.hasher.Hash(params.Password)
	if err != nil {
		return err
	}

	u := &User{
		Username: params.Username,
		Email:    params.Email,
		Password: hashed,
	}

	if err := us.userDB.AddUser(u); err != nil {
		return err
	}

	key, err := NewKeyPair(params.Username, params.Password)
	if err != nil {
		return err
	}

	if err := us.keyDB.AddUserKey(u, key); err != nil {
		return err
	}

	return nil
}

// UpdateUser updates a user to the new values given. If the field of newValue
// is a zero value, it is ignored. If a new password is specified, the user's
// GPG key is re-encrypted under the new password.
func (us *UserService) UpdateUser(username, oldPassword string, newValue *User) error {
	user, err := us.userDB.GetUser(username)
	if err != nil {
		return fmt.Errorf("user '%s': %w", username, ErrNotFound)
	}

	if newValue.Password != "" {
		user.Password = newValue.Password

		var hashed string

		hashed, err = us.hasher.Hash(user.Password)
		if err != nil {
			return fmt.Errorf("%w: %v", ErrHashFail, err)
		}

		user.Password = hashed
	}

	if newValue.Email != "" {
		user.Email = newValue.Email
	}

	// TODO only execute the key rotation steps if a new password is specified.
	key, err := us.keyDB.GetUserKey(user)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrBackendError, err)
	}

	err = UpdateKey(key, newValue.Password, oldPassword)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrBackendError, err)
	}

	// TODO we NEED to be able to roll this back if we get a failure later down
	// the line. This should absolutely be done in a transaction.
	if err = us.keyDB.UpdateUserKey(key); err != nil {
		return fmt.Errorf("%w: couldn't update key for user '%s': %v", ErrBackendError, username, err)
	}

	if err := us.userDB.UpdateUser(user); err != nil {
		return fmt.Errorf("%w: couldn't update user '%s': %v", ErrBackendError, username, err)
	}

	return nil
}
