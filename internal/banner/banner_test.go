package banner_test

import (
	"flag"
	"testing"

	. "gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

//nolint:gochecknoglobals // This is a test flag
var update = flag.Bool("update", false, "update the golden files")

func TestBuilder_MakeHTMLBanner(t *testing.T) {
	type args struct {
		mailboxID string
	}

	tests := []struct {
		name    string
		args    args
		builder *Builder
		want    string
		wantErr bool
	}{
		{
			name: "Builds a banner given an ID",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
			),
		},
		{
			name: "Excludes mailbox disable link if hostname not specified",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL(""),
			),
		},
		{
			name: "Excludes mailbox disable link if ID not specified",
			args: args{
				mailboxID: "",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
			),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.builder.MakeHTMLBanner(tt.args.mailboxID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Builder.MakeHTMLBanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			test.AssertMatchesGoldenFile(t, got, tt.name, ".html", update)
		})
	}
}

func TestBuilder_MakeTextBanner(t *testing.T) {
	type args struct {
		mailboxID string
	}

	tests := []struct {
		name    string
		args    args
		builder *Builder
		want    string
		wantErr bool
	}{
		{
			name: "Builds a text banner given an ID",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
			),
		},
		{
			name: "Builds a text banner without an ID",
			args: args{
				mailboxID: "",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
			),
		},
		{
			name: "Builds a text banner without a URL",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL(""),
			),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.builder.MakeTextBanner(tt.args.mailboxID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Builder.MakeHTMLBanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			test.AssertMatchesGoldenFile(t, got, tt.name, ".txt", update)
		})
	}
}
