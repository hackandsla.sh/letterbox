package db

import (
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func (db *SQLDatastore) AddUser(u *letterbox.User) error {
	if err := db.DB.Create(u).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) GetUser(name string) (*letterbox.User, error) {
	u := &letterbox.User{}
	if err := db.DB.Where(&letterbox.User{Username: name}).First(&u).Error; err != nil {
		return nil, err
	}

	return u, nil
}

func (db *SQLDatastore) UpdateUser(u *letterbox.User) error {
	if err := db.DB.Save(u).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) GetAllUsers() ([]letterbox.User, error) {
	users := []letterbox.User{}
	if err := db.DB.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (db *SQLDatastore) GetUserByID(id uint) (*letterbox.User, error) {
	var q letterbox.User
	q.ID = id

	var user letterbox.User
	if err := db.DB.Where(&q).First(&user).Error; err != nil {
		return nil, wrapGormErr(err)
	}

	return &user, nil
}
