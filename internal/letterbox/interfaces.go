package letterbox

//go:generate $COUNTERFEITER -generate

// Datastore embeds the datastores for each domain object.
//counterfeiter:generate . Datastore
type Datastore interface {
	DomainStore
	UserStore
	UserIDStore
	UserKeyStore
	MailboxStore
	MailboxStatStore
	MessageStore
}
