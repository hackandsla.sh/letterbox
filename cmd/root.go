package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/server"
)

// Execute the root command.
func Execute(c letterbox.Info) {
	var rootCmd = initRootCmd(c)
	rootCmd.Version = c.Version

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initRootCmd(i letterbox.Info) *cobra.Command {
	var customConfigFile string

	cobra.OnInitialize(getInitConfig(&customConfigFile))

	cmd := &cobra.Command{
		Use:   "letterbox",
		Short: "letterbox helps do stuff",
		Long:  "letterbox helps do stuff",
		Run: func(cmd *cobra.Command, args []string) {
			config := getConfig()
			s := server.New(config, &i)
			if err := s.Start(); err != nil {
				log.Fatalf("error during server startup: %v", err)
			}
		},
	}
	bindFlags(cmd, &customConfigFile)
	addSubcommands(cmd)

	return cmd
}

func getConfig() *server.Config {
	c := &server.Config{}

	err := viper.Unmarshal(c)
	if err != nil {
		log.Fatalf("Could not read configuration: %s", err.Error())
	}

	if err := c.Validate(); err != nil {
		log.Fatalf("Invalid configuration: %s", err.Error())
	}

	return c
}

//nolint:wsl,funlen // All statements should cuddle their associated assignments
func bindFlags(cmd *cobra.Command, customConfigFile *string) {
	cmd.PersistentFlags().StringVar(customConfigFile, "config", "", "config file")

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	cmd.PersistentFlags().BoolP("verbose", "v", false, "Run with verbose logs")
	_ = viper.BindPFlag("log.verbose", cmd.PersistentFlags().Lookup("verbose"))

	cmd.PersistentFlags().StringP("log-file", "l", "", "File to log to")
	_ = viper.BindPFlag("log.file", cmd.PersistentFlags().Lookup("log-file"))

	cmd.PersistentFlags().Bool("development", false, "Enable development mode")
	_ = viper.BindPFlag("log.development", cmd.PersistentFlags().Lookup("development"))

	cmd.PersistentFlags().String("http-hostname", "", "HTTP hostname to use")
	_ = viper.BindPFlag("http.hostname", cmd.PersistentFlags().Lookup("http-hostname"))

	cmd.PersistentFlags().IntP("port", "p", 8080, "Port to run HTTP server on")
	_ = viper.BindPFlag("http.port", cmd.PersistentFlags().Lookup("port"))

	cmd.PersistentFlags().Int("max-shutdown-seconds", 30, "The max time (in seconds) to wait for the HTTP server to shut down")
	_ = viper.BindPFlag("http.MaxShutdownTime", cmd.PersistentFlags().Lookup("port"))

	cmd.PersistentFlags().String("http-max-body-size", "1MB", "The max size of HTTP request bodies")
	_ = viper.BindPFlag("http.MaxBodyReadSize", cmd.PersistentFlags().Lookup("http-max-body-size"))

	cmd.PersistentFlags().String("db-user", "letterbox", "Specify database user")
	_ = viper.BindPFlag("db.user", cmd.PersistentFlags().Lookup("db-user"))

	cmd.PersistentFlags().String("db-password", "", "Specify database password. ONLY USE THE ON THE CLI FOR TESTING")
	_ = viper.BindPFlag("db.password", cmd.PersistentFlags().Lookup("db-password"))

	cmd.PersistentFlags().String("db-database", "letterbox", "Specify the database to use")
	_ = viper.BindPFlag("db.database", cmd.PersistentFlags().Lookup("db-database"))

	cmd.PersistentFlags().String("db-host", "", "Specify the database host to use")
	_ = viper.BindPFlag("db.host", cmd.PersistentFlags().Lookup("db-host"))

	cmd.PersistentFlags().Int("db-port", 0, "Specify the database port to use")
	_ = viper.BindPFlag("db.port", cmd.PersistentFlags().Lookup("db-port"))

	cmd.PersistentFlags().Bool("db-insecure", false, "If specified, will not use SSL to connect to the database")
	_ = viper.BindPFlag("db.insecure", cmd.PersistentFlags().Lookup("db-insecure"))

	cmd.PersistentFlags().Int("smtp-port", 587, "Specify the SMTP port to use")
	_ = viper.BindPFlag("smtp.port", cmd.PersistentFlags().Lookup("smtp-port"))

	cmd.PersistentFlags().String("smtp-max-size", "10MB", "The maximum email size to accept")
	_ = viper.BindPFlag("smtp.MaxSize", cmd.PersistentFlags().Lookup("smtp-max-size"))

	cmd.PersistentFlags().String("smtp-outbound-host", "", "The host to use to send proxied emails")
	_ = viper.BindPFlag("smtp.outbound.host", cmd.PersistentFlags().Lookup("smtp-outbound-host"))

	cmd.PersistentFlags().Int("smtp-outbound-port", 587, "The port to use to send proxied emails")
	_ = viper.BindPFlag("smtp.outbound.port", cmd.PersistentFlags().Lookup("smtp-outbound-port"))

	cmd.PersistentFlags().String("smtp-outbound-username", "", "The username to authenticate to the outbound SMTP server with")
	_ = viper.BindPFlag("smtp.outbound.username", cmd.PersistentFlags().Lookup("smtp-outbound-username"))

	cmd.PersistentFlags().String("smtp-outbound-password", "", "The password to authenticate to the outbound SMTP server with")
	_ = viper.BindPFlag("smtp.outbound.password", cmd.PersistentFlags().Lookup("smtp-outbound-password"))

	cmd.PersistentFlags().String("smtp-hostname", "localhost", "The hostname to use on the SMTP server")
	_ = viper.BindPFlag("smtp.Hostname", cmd.PersistentFlags().Lookup("smtp-hostname"))

	cmd.PersistentFlags().String("smtp-tls-cert", "", "The TLS certificate string to use")
	_ = viper.BindPFlag("smtp.TLS.Cert", cmd.PersistentFlags().Lookup("smtp-tls-cert"))

	cmd.PersistentFlags().String("smtp-tls-key", "", "The TLS key string to use")
	_ = viper.BindPFlag("smtp.TLS.Key", cmd.PersistentFlags().Lookup("smtp-tls-key"))

	cmd.PersistentFlags().String("smtp-tls-certfile", "", "The TLS certificate file to use")
	_ = viper.BindPFlag("smtp.TLS.CertFile", cmd.PersistentFlags().Lookup("smtp-tls-certfile"))

	cmd.PersistentFlags().String("smtp-tls-keyfile", "", "The TLS key file to use")
	_ = viper.BindPFlag("smtp.TLS.KeyFile", cmd.PersistentFlags().Lookup("smtp-tls-keyfile"))

	cmd.PersistentFlags().Int("smtp-tls-port", 465, "The port to use for the implicit TLS server (see RFC8314)")
	_ = viper.BindPFlag("smtp.TLS.Port", cmd.PersistentFlags().Lookup("smtp-tls-port"))

	cmd.PersistentFlags().String("init-username", "", "The initial admin username")
	_ = viper.BindPFlag("init.username", cmd.PersistentFlags().Lookup("init-username"))

	cmd.PersistentFlags().String("init-password", "", "The initial admin password. "+
		"If specified, letterbox will attempt to create the initial admin account. "+
		"Otherwise, an admin account will have to be specified manually.")
	_ = viper.BindPFlag("init.password", cmd.PersistentFlags().Lookup("init-password"))

	cmd.PersistentFlags().Int("max-auth-attempts", 10, "The allowed number of auth attempts before the IP is banned")
	_ = viper.BindPFlag("badactor.MaxAuthAttempts", cmd.PersistentFlags().Lookup("max-auth-attempts"))

	//nolint:gomnd // This is the config default
	cmd.PersistentFlags().Duration("jail-time", time.Minute*10, "The amount of time an IP gets banned for")
	_ = viper.BindPFlag("badactor.JailTime", cmd.PersistentFlags().Lookup("jail-time"))

	cmd.PersistentFlags().Duration("cleanup-interval", time.Minute*1, "The time between expired mailbox cleanup")
	_ = viper.BindPFlag("cleanup.Interval", cmd.PersistentFlags().Lookup("cleanup-interval"))

	cmd.PersistentFlags().String("banner-url", "", "The URL to use for redirection links")
	_ = viper.BindPFlag("banner.URL", cmd.PersistentFlags().Lookup("banner-url"))

	cmd.PersistentFlags().String("banner-html-template", banner.DefaultHTMLBanner, "The template to use on banners in forwarded HTML mail")
	_ = viper.BindPFlag("banner.HTMLTemplate", cmd.PersistentFlags().Lookup("banner-html-template"))

	cmd.PersistentFlags().String("banner-text-template", banner.DefaultTextBanner, "The template to use on banners in forwarded text mail")
	_ = viper.BindPFlag("banner.TextTemplate", cmd.PersistentFlags().Lookup("banner-text-template"))
}

func addSubcommands(cmd *cobra.Command) {
	cmd.AddCommand(newCmdGenCompletion(cmd))
	cmd.AddCommand(newCmdGenDocs(cmd))
	cmd.AddCommand(newCmdInit())
	cmd.AddCommand(newCmdSMTPCLI())
}

// getInitConfig creates a small closure which binds a potential custom configuration file.
// Since we don't know at the start whether this is needed.
func getInitConfig(customConfigFile *string) func() {
	return func() {
		// Don't forget to read config either from cfgFile or from home directory!
		if (*customConfigFile) != "" {
			// Use config file from the flag.
			viper.SetConfigFile(*customConfigFile)

			if err := viper.ReadInConfig(); err != nil {
				log.Printf("Can't read custom config '%s': %e", *customConfigFile, err)
				os.Exit(1)
			}
		} else {
			viper.SetConfigName("letterbox")               // name of config file (without extension)
			viper.AddConfigPath(".")                       // optionally look for config in the working directory
			viper.AddConfigPath("$HOME/.config/letterbox") // call multiple times to add many search paths
			viper.AddConfigPath("/etc/letterbox/")         // path to look for the config file in
			_ = viper.ReadInConfig()                       // Find and read the config file
		}
	}
}
