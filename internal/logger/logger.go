package logger

//go:generate $COUNTERFEITER -generate

//counterfeiter:generate . Logger
type Logger interface {
	// Infow logs messages that are relevant for administration or monitoring.
	Infow(msg string, fields ...Fields)
	// Debugw logs messages that are important for development but unimpoartant
	// for administration or monitoring.
	Debugw(msg string, fields ...Fields)
	// Errorw logs critical errors (e.g. startup errors, errors that should
	// never be encountered, etc).
	Errorw(msg string, err error, fields ...Fields)
	// InfoErr is used for non-critical errors. This should be used in most
	// cases instead of Errorw.
	InfoErr(msg string, err error, fields ...Fields)
	// With creates a new child logger with the given fields built-in.
	With(fields Fields) Logger
}

type Fields map[string]interface{}
