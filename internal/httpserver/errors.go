package httpserver

import "errors"

var (
	ErrUnauthorized     = errors.New("unauthorized")
	ErrServiceUnhealthy = errors.New("HTTP service unhealthy")
	ErrDecode           = errors.New("can't decode JSON message")
)
