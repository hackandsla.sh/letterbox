package cleanup

import (
	"errors"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

var ErrMissingDependency = errors.New("missing dependency")

type Config struct {
	Interval time.Duration `json:"interval,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Interval, validation.Required),
	)
}

type Cleanup struct {
	quit   chan interface{}
	config *Config

	mailboxes *letterbox.MailboxService
	log       logger.Logger
}

func New(config *Config, log logger.Logger, mailboxes *letterbox.MailboxService) *Cleanup {
	return &Cleanup{
		config:    config,
		log:       log,
		mailboxes: mailboxes,
	}
}

func (cs *Cleanup) Name() string {
	return "Cleanup"
}

func (cs *Cleanup) Start() error {
	ticker := time.NewTicker(cs.config.Interval)
	cs.quit = make(chan interface{})

	go func() {
		for {
			select {
			case <-ticker.C:
				if err := cs.mailboxes.DeleteExpiredMailboxes(cs.log); err != nil {
					cs.log.InfoErr("error while removing expired mailboxes", err)
				}
			case <-cs.quit:
				ticker.Stop()
				return
			}
		}
	}()

	return nil
}

func (cs *Cleanup) Stop() error {
	close(cs.quit)
	return nil
}
