package httpserver

import (
	"errors"
	"fmt"
	"net/http"
	"testing"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func Test_getStatusCodeForError(t *testing.T) {
	type args struct {
		err error
	}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Match a status code with a known error",
			args: args{
				err: fmt.Errorf("known error: %w", letterbox.ErrNotFound),
			},
			want: http.StatusNotFound,
		},
		{
			name: "Match an 'internal service error' with an unknown error",
			args: args{
				err: errors.New("unknown error"),
			},
			want: http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getStatusCodeForError(tt.args.err); got != tt.want {
				t.Errorf("getStatusCodeForError() = %v, want %v", got, tt.want)
			}
		})
	}
}
