package letterbox_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestMailboxStatsService_IncrementOrInsertStat(t *testing.T) {
	var mailboxID uint = 1

	type args struct {
		domain string
	}

	tests := []struct {
		name       string
		args       args
		dbSetup    test.DBOption
		dbValidate func(t *testing.T, db *letterboxfakes.FakeDatastore)
		wantErr    bool
	}{
		{
			name: "Should increment an existing stats record",
			args: args{
				domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetStatReturns(&letterbox.DomainStat{Domain: "example.com", Count: 10}, nil)
			},
			dbValidate: func(t *testing.T, db *letterboxfakes.FakeDatastore) {
				assert.Equal(t, 1, db.UpdateStatCallCount())
				assert.Equal(t, 0, db.CreateStatCallCount())
				updatedStat := db.UpdateStatArgsForCall(0)
				assert.Equal(t, uint(11), updatedStat.Count)
			},
		},
		{
			name: "Should create a stats record if it doesn't exist",
			args: args{
				domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetStatReturns(nil, letterbox.ErrNotFound)
			},
			dbValidate: func(t *testing.T, db *letterboxfakes.FakeDatastore) {
				assert.Equal(t, 0, db.UpdateStatCallCount())
				assert.Equal(t, 1, db.CreateStatCallCount())
				createdStat := db.CreateStatArgsForCall(0)
				assert.Equal(t, uint(1), createdStat.Count)
			},
		},
		{
			name: "Should error if the stats fetch error isn't a RecordNotFound error",
			args: args{
				domain: "example.com",
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetStatReturns(nil, letterbox.ErrBackendError)
			},
			dbValidate: func(t *testing.T, db *letterboxfakes.FakeDatastore) {
				assert.Equal(t, 0, db.UpdateStatCallCount())
				assert.Equal(t, 0, db.CreateStatCallCount())
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			db := test.DB(tt.dbSetup)
			stats := letterbox.NewMailboxStatsService(db)

			// Act
			err := stats.IncrementOrInsertStat(mailboxID, tt.args.domain)

			// Assert
			if (err != nil) != tt.wantErr {
				t.Errorf("IncrementOrInsertStat() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.dbValidate != nil {
				tt.dbValidate(t, db)
			}
		})
	}
}
