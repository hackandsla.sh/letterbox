package smtpclient

import (
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type NopClient struct {
	log logger.Logger
}

func NewNopClient(log logger.Logger) *NopClient {
	return &NopClient{
		log: log,
	}
}

func (nop *NopClient) SendMail(message *letterbox.ForwardingMessage) error {
	nop.log.Debugw("would send SMTP message", logger.Fields{
		"from":     message.FromHeader,
		"to":       message.ToHeader,
		"subject":  message.Subject,
		"htmlBody": message.HTMLBody,
		"textBody": message.TextBody,
	})

	return nil
}
