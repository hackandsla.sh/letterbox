package httpserver

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"sync/atomic"
	"time"

	"github.com/docker/go-units"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/jaredfolkins/badactor"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/validate"
)

type Config struct {
	MaxShutdownTime time.Duration `json:"max_shutdown_time,omitempty"`
	MaxBodyReadSize string        `json:"max_body_read_size,omitempty"`
	Port            int           `json:"port,omitempty"`
}

func (c *Config) Validate() error {
	const tcpMax int = 65535

	return validation.ValidateStruct(c,
		validation.Field(&c.Port, validation.Required, validation.Min(1), validation.Max(tcpMax)),
		validation.Field(&c.MaxBodyReadSize, validation.Required, validation.By(validate.IsBytesSize)),
	)
}

type HTTPService struct {
	httpServer      *http.Server
	isHealthy       int32
	maxShutdownTime time.Duration
	maxBodyReadSize int64

	// Service Dependencies
	db           letterbox.Datastore
	log          logger.Logger
	serverInfo   *letterbox.Info
	hasher       letterbox.ArgonHasher
	badActors    *badactor.Studio
	domains      *letterbox.DomainService
	mailboxes    *letterbox.MailboxService
	messages     *letterbox.MessageService
	users        *letterbox.UserService
	statsSummary StatsSummarizer
}

func New(
	config *Config,
	db letterbox.Datastore,
	log logger.Logger,
	info *letterbox.Info,
	hasher letterbox.ArgonHasher,
	badActors *badactor.Studio,
	domains *letterbox.DomainService,
	mailboxes *letterbox.MailboxService,
	messages *letterbox.MessageService,
	users *letterbox.UserService,
	statsSummary StatsSummarizer,
) *HTTPService {
	s := &HTTPService{
		maxShutdownTime: config.MaxShutdownTime,
		db:              db,
		log:             log,
		serverInfo:      info,
		hasher:          hasher,
		badActors:       badActors,
		domains:         domains,
		mailboxes:       mailboxes,
		messages:        messages,
		users:           users,
		statsSummary:    statsSummary,
	}

	// Ignoring this error, since it's already validated during Validate()
	s.maxBodyReadSize, _ = units.FromHumanSize(config.MaxBodyReadSize)

	root := NewRouter(s.log)
	root.Use(newSecurityMiddleware())
	root.Use(newBadActorMiddleware(badActors, log))
	root.UsePerRoute(newMetricsMiddleware())

	s.initRoutes(root)

	listenAddr := ":" + strconv.Itoa(config.Port)

	s.httpServer = &http.Server{
		Addr:         listenAddr,
		Handler:      root,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	atomic.StoreInt32(&s.isHealthy, 1)

	return s
}

func (s *HTTPService) Name() string {
	return "HTTP"
}

func (s *HTTPService) Start() error {
	log := s.log.With(logger.Fields{
		"address": s.httpServer.Addr,
	})

	log.Infow("starting server")
	// TODO use a channel to propagate the error back up to the initializer
	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Errorw("could not listen on address", err)
		}
	}()

	log.Infow("server is ready to handle requests")

	return nil
}

func (s *HTTPService) Stop() error {
	atomic.StoreInt32(&s.isHealthy, 0)

	ctx, cancel := context.WithTimeout(context.Background(), s.maxShutdownTime)
	defer cancel()

	s.httpServer.SetKeepAlivesEnabled(false)

	if err := s.httpServer.Shutdown(ctx); err != nil {
		s.log.InfoErr("could not gracefully shutdown the server", err)
		return err
	}

	return nil
}

func wrapHandler(h HandlerFunc, log logger.Logger) *Handler {
	return &Handler{
		Handler: h,
		log:     log,
	}
}

type StatsSummarizer interface {
	GetStats(mailbox *letterbox.Mailbox) ([]letterbox.DomainStat, error)
}
