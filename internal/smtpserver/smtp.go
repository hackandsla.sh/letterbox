package smtpserver

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"time"

	smtp "github.com/emersion/go-smtp"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/dkim"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/parsemail"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/spf"
	"gitlab.com/hackandsla.sh/letterbox/internal/wildcard"
)

// TODO research valid SMTP codes.
var (
	ErrSMTPInvalidDestination = &smtp.SMTPError{
		Code:         552,
		EnhancedCode: smtp.EnhancedCode{5, 3, 4},
		Message:      "Invalid destination",
	}

	ErrSMTPServerError = &smtp.SMTPError{
		Code:         552,
		EnhancedCode: smtp.EnhancedCode{5, 3, 4},
		Message:      "Server error",
	}
)

const (
	SecurityPass string = "pass"
	SecurityFail string = "fail"
)

//go:generate $COUNTERFEITER -generate
//counterfeiter:generate . EmailSender
type EmailSender interface {
	SendMail(*letterbox.ForwardingMessage) error
}

//counterfeiter:generate . dkimChecker
type dkimChecker interface {
	GetStatus(raw []byte) (dkim.Status, error)
	ValidateAlignment(raw []byte, from string) (bool, error)
}

//counterfeiter:generate . spfChecker
type spfChecker interface {
	GetStatus(remoteIP net.IP, helo, fromHeader string) (spf.Status, error)
	ValidateAlignment(from, fromHeader string) (bool, error)
}

// Login handles a login command with username and password.
func (ss *SMTPService) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	return nil, smtp.ErrAuthUnsupported
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails.
func (ss *SMTPService) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return &Session{
		DB:         ss.db,
		SMTPClient: ss.smtpClient,
		DKIM:       ss.dkim,
		SPF:        ss.spf,
		Log:        ss.log,
		Banner:     ss.banner,
		MaxSize:    ss.maxSize,
		Mailboxes:  ss.mailboxes,
		Messages:   ss.messages,
		Stats:      ss.stats,

		RemoteAddr: state.RemoteAddr,
		Helo:       state.Hostname,
	}, nil
}

type Session struct {
	SMTPClient EmailSender
	DB         letterbox.Datastore
	DKIM       dkimChecker
	SPF        spfChecker
	Log        logger.Logger
	Banner     *banner.Builder
	MaxSize    int64
	Mailboxes  *letterbox.MailboxService
	Messages   *letterbox.MessageService
	Stats      *letterbox.MailboxStatsService

	RemoteAddr net.Addr
	Helo       string

	From    string
	To      string
	Mailbox *letterbox.Mailbox
}

func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	s.From = from
	return nil
}

func (s *Session) Rcpt(to string) error {
	var err error

	s.Mailbox, err = s.Mailboxes.GetOrCreateMailboxForAddress(to)
	if err != nil {
		s.Log.InfoErr("invalid destination inbox attempted", err,
			logger.Fields{
				"address": to,
			},
		)

		return ErrSMTPInvalidDestination
	}

	s.To = to

	return nil
}

func (s *Session) Data(r io.Reader) error {
	log := s.Log.With(
		logger.Fields{
			"ip":   s.RemoteAddr.String(),
			"from": s.From,
			"to":   s.To,
		},
	)

	message := &letterbox.Message{
		From:         s.From,
		To:           s.To,
		ReceivedTime: time.Now(),
	}

	remoteIP, err := getIP(s.RemoteAddr)
	if err != nil {
		log.InfoErr("could not parse remote IP", err)

		return ErrSMTPServerError
	}

	raw, err := LimitedRead(r, s.MaxSize)
	if err != nil {
		log.InfoErr("could not read message", err)

		return ErrSMTPServerError
	}

	message.Raw = string(raw)

	parsedEmail, err := parsemail.Parse(bytes.NewReader(raw))
	if err != nil {
		log.InfoErr("could not parse message", err)
		return ErrSMTPServerError
	}

	message.ToHeader = parsedEmail.Header.Get("To")
	message.Subject = parsedEmail.Subject
	message.DateHeader = parsedEmail.Date

	message.FromHeader = parsedEmail.Header.Get("From")
	if message.FromHeader == "" {
		log.InfoErr("no 'from' header provided", err)
		return ErrSMTPServerError
	}

	message.HTMLBody = parsedEmail.HTMLBody
	message.TextBody = parsedEmail.TextBody

	splitFrom, err := letterbox.SplitAddr(message.FromHeader)
	if err != nil {
		log.InfoErr("error while parsing 'from' header", err)
		return ErrSMTPServerError
	}

	if whitelistErr := validateDomainAllowlist(s.Mailbox.DomainWhitelist, splitFrom.Domain); whitelistErr != nil {
		log.Infow("no matching domain found in whitelist",
			logger.Fields{
				"domain": splitFrom.Domain,
			},
		)

		return ErrSMTPServerError
	}

	message.SPFResult = getSPFStatus(remoteIP, s.Helo, splitFrom.Full, s.SPF, log)
	message.SPFAlignment = getSPFAlignment(message.From, splitFrom.Full, s.SPF, log)
	message.DKIMStatus = getDKIMStatus([]byte(message.Raw), s.DKIM, log)
	message.DKIMAlignment = getDKIMAlignment([]byte(message.Raw), splitFrom.Full, s.DKIM, log)

	// TODO only increment stats if this email passes our defined security
	// policy For example, we wouldn't want to increment our domain statistics
	// if we believe to a reasonable degree of confidence that this email was
	// spoofed.
	err = s.Stats.IncrementOrInsertStat(s.Mailbox.ID, splitFrom.Domain)
	if err != nil {
		log.InfoErr("error while incrementing stats for domain", err)
	}

	if err = s.Messages.AddMessage(message, s.Mailbox); err != nil {
		log.InfoErr("could not add message to database", err)
		return ErrSMTPServerError
	}

	proxyMessage, err := letterbox.NewProxyMessage(message, s.Mailbox, log, s.DB, s.Banner)
	if err != nil {
		return err
	}

	if proxyMessage != nil {
		if err := s.SMTPClient.SendMail(proxyMessage); err != nil {
			log.InfoErr("error while sending proxy email", err)
		}
	}

	log.Debugw("adding message")

	return nil
}

func (s *Session) Reset() {}

func (s *Session) Logout() error {
	return nil
}

func validateDomainAllowlist(allowlist []string, domain string) error {
	if len(allowlist) > 0 {
		foundMatchingDomain := false

		for _, d := range allowlist {
			if wildcard.Match(d, domain) {
				foundMatchingDomain = true
			}
		}

		if !foundMatchingDomain {
			return fmt.Errorf("%w: no matching domain found for domain '%s'", ErrInvalidDestination, domain)
		}
	}

	return nil
}

func getSPFStatus(remoteIP net.IP, helo, fromHeader string, spfCheck spfChecker, log logger.Logger) string {
	status, err := spfCheck.GetStatus(remoteIP, helo, fromHeader)
	if err != nil {
		log.InfoErr("SPF check failed", err)
	}

	return status.String()
}

func getSPFAlignment(from, fromHeader string, spfCheck spfChecker, log logger.Logger) string {
	alignment, err := spfCheck.ValidateAlignment(from, fromHeader)
	if err != nil {
		log.InfoErr("SPF alignment check failed", err)
	}

	status := SecurityFail
	if alignment {
		status = SecurityPass
	}

	return status
}

func getDKIMStatus(raw []byte, dkimCheck dkimChecker, log logger.Logger) string {
	status, err := dkimCheck.GetStatus(raw)
	if err != nil {
		log.InfoErr("DKIM check failed", err)
	}

	return status.String()
}

func getDKIMAlignment(raw []byte, fromHeader string, dkimCheck dkimChecker, log logger.Logger) string {
	alignment, err := dkimCheck.ValidateAlignment(raw, fromHeader)
	if err != nil {
		log.InfoErr("DKIM alignment check failed", err)
	}

	ret := SecurityFail
	if alignment {
		ret = SecurityPass
	}

	return ret
}

//nolint:interfacer // We only expect to use net.Addr structs here
func getIP(a net.Addr) (net.IP, error) {
	remoteHost, _, err := net.SplitHostPort(a.String())
	if err != nil {
		return nil, fmt.Errorf("%w: couldn't interpret remote host '%s'", ErrParsingError, a.String())
	}

	remoteIP := net.ParseIP(remoteHost)
	if remoteIP == nil {
		return nil, fmt.Errorf("%w: couldn't parse IP '%s'", ErrParsingError, remoteHost)
	}

	return remoteIP, nil
}

// A limitedReader reads from R but limits the amount of
// data returned to just N bytes. Each call to Read
// updates N to reflect the new amount remaining.
// Read returns ErrExceedMaxSize when N <= 0 or EOF when the underlying R returns EOF.
//
// This differs from a LimitedReader only in that this will raise
// a different error after the max size has been reached, allowing
// the connection to be dropped.
type limitedReader struct {
	R io.Reader // underlying reader
	N int64     // max bytes remaining
}

func (l *limitedReader) Read(p []byte) (n int, err error) {
	if l.N <= 0 {
		return 0, ErrMaxSizeExceeded
	}

	if int64(len(p)) > l.N {
		p = p[0:l.N]
	}

	n, err = l.R.Read(p)
	l.N -= int64(n)

	return
}

// LimitedRead validates a mail message is no more than 'size' bytes,
// and returns a new io.Reader containing a copy of the original bytes.
func LimitedRead(r io.Reader, size int64) ([]byte, error) {
	limited := &limitedReader{
		R: r,
		N: size,
	}

	raw, err := ioutil.ReadAll(limited)
	if err != nil {
		return nil, err
	}

	return raw, nil
}
