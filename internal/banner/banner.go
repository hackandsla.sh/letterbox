package banner

import (
	"html/template"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type Config struct {
	// The public URI to use with redirection links
	URL string

	// The template to use for HTML banners
	HTMLTemplate string

	// The template to use for text banners
	TextTemplate string
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.HTMLTemplate, validation.Required),
		validation.Field(&c.TextTemplate, validation.Required),
	)
}

type Builder struct {
	url          string
	htmlTemplate *template.Template
	textTemplate *template.Template
}

const DefaultHTMLBanner = `
<div style="background-color: #EEE; color: #333; padding: 1em">
	<p>This mail was proxied by Letterbox</p>
	{{- if and (ne .URL "") (ne .ID "") }}
	<br />
	<a href="{{ .URL }}/disable/{{ .ID }}">Disable Mailbox</a>
	{{- end }}
</div>
`

const DefaultTextBanner = `
-------------------------------------------------
This mail was proxied by Letterbox
{{- if and (ne .URL "") (ne .ID "") }}

Disable: {{ .URL }}/disable/{{ .ID }}
{{- end }}
-------------------------------------------------
`

func NewBuilder(config *Config) (*Builder, error) {
	htmlTemplate, err := template.New("htmlBanner").Parse(config.HTMLTemplate)
	if err != nil {
		return nil, err
	}

	textTemplate, err := template.New("textBanner").Parse(config.TextTemplate)
	if err != nil {
		return nil, err
	}

	return &Builder{
		url:          config.URL,
		htmlTemplate: htmlTemplate,
		textTemplate: textTemplate,
	}, nil
}

type templateVars struct {
	URL string
	ID  string
}

func (b *Builder) MakeHTMLBanner(mailboxID string) (string, error) {
	return b.makeBanner(mailboxID, b.htmlTemplate)
}

func (b *Builder) MakeTextBanner(mailboxID string) (string, error) {
	return b.makeBanner(mailboxID, b.textTemplate)
}

func (b *Builder) makeBanner(mailboxID string, tmpl *template.Template) (string, error) {
	tv := templateVars{
		URL: b.url,
		ID:  mailboxID,
	}

	var builder strings.Builder

	if err := tmpl.Execute(&builder, tv); err != nil {
		return "", err
	}

	return builder.String(), nil
}
