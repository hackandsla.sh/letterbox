package db

import "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"

func (db *SQLDatastore) AddDomain(d *letterbox.Domain) error {
	return db.DB.Create(d).Error
}

func (db *SQLDatastore) GetDomain(name string) (*letterbox.Domain, error) {
	d := &letterbox.Domain{}
	if err := db.DB.Where(&letterbox.Domain{Domain: name}).First(d).Error; err != nil {
		return nil, wrapGormErr(err)
	}

	return d, nil
}

func (db *SQLDatastore) GetAllDomains() ([]letterbox.Domain, error) {
	list := []letterbox.Domain{}
	if err := db.DB.Find(&list).Error; err != nil {
		return nil, err
	}

	return list, nil
}

func (db *SQLDatastore) DeleteDomain(d *letterbox.Domain) error {
	if d.ID == 0 {
		return letterbox.ErrMissingID
	}

	return db.DB.Unscoped().Delete(d).Error
}
