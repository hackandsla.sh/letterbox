package zap

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

// Config defines the configurations needed by our logger.
type Config struct {
	LogFile     string `json:"file,omitempty"`
	Verbose     bool   `json:"verbose,omitempty"`
	Development bool   `json:"development,omitempty"`
}

// Validate validates whether our logger config is valid.
func (c *Config) Validate() error {
	return nil
}

// Logger defines our logging service.
type Logger struct {
	*zap.SugaredLogger
}

// New builds a new logger from a given config.
func New(c *Config) (*Logger, error) {
	var zapConfig zap.Config
	if c.Development {
		zapConfig = zap.NewDevelopmentConfig()
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		zapConfig.EncoderConfig.EncodeDuration = zapcore.SecondsDurationEncoder
	} else {
		zapConfig = zap.NewProductionConfig()
	}

	if c.LogFile != "" {
		zapConfig.OutputPaths = append(zapConfig.OutputPaths, c.LogFile)
	}

	if c.Verbose {
		zapConfig.Level.SetLevel(zapcore.DebugLevel)
	}

	log, err := zapConfig.Build()
	if err != nil {
		return nil, err
	}

	sl := log.Sugar()

	if c.LogFile != "" {
		sl.Debugw(
			"using logfile",
			"path", c.LogFile,
		)
	}

	return &Logger{
		SugaredLogger: sl,
	}, nil
}

// Name returns the name of this service.
func (l *Logger) Name() string {
	return "Logger"
}

// Start indicates that the server is starting.
func (l *Logger) Start() error {
	l.Debug("Starting server in debug mode")
	return nil
}

// Stop flushes writes to any open log files.
func (l *Logger) Stop() error {
	_ = l.Sync() // Ignore this error
	return nil
}

func (l *Logger) With(fields logger.Fields) logger.Logger {
	return &Logger{
		SugaredLogger: l.SugaredLogger.With(flattenFields([]logger.Fields{fields})...),
	}
}

// InfoErr is to log non-critical errors. It adds a new field called "error"
// which contains the content of the given error.
func (l *Logger) InfoErr(msg string, err error, fields ...logger.Fields) {
	fields = append(fields,
		logger.Fields{
			"error": err,
		})

	l.SugaredLogger.Infow(msg, flattenFields(fields)...)
}

func (l *Logger) Infow(msg string, fields ...logger.Fields) {
	l.SugaredLogger.Infow(msg, flattenFields(fields)...)
}

func (l *Logger) Errorw(msg string, err error, fields ...logger.Fields) {
	fields = append(fields,
		logger.Fields{
			"error": err,
		})

	l.SugaredLogger.Errorw(msg, flattenFields(fields)...)
}

func (l *Logger) Debugw(msg string, fields ...logger.Fields) {
	l.SugaredLogger.Debugw(msg, flattenFields(fields)...)
}

func flattenFields(fields []logger.Fields) []interface{} {
	combined := logger.Fields{}

	for _, f := range fields {
		for k, v := range f {
			combined[k] = v
		}
	}

	var flattened []interface{}
	for k, v := range combined {
		flattened = append(flattened, k, v)
	}

	return flattened
}
