package httpserver

import (
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

func NewRouter(log logger.Logger) *SubRouter {
	return &SubRouter{
		router: httprouter.New(),
		log:    log,
	}
}

type SubRouter struct {
	// The full path prefix that this SubRouter is adding to
	path string
	// The HTTP router
	router *httprouter.Router
	// The middleware to add at the route level
	perRouteMiddlewares []PerRouteMiddleware
	// Middleware to append to all child routes. These get appended to an extra subroutes
	middlewares []Middleware
	// The logger to pass to each handler
	log logger.Logger
}

func (s *SubRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *SubRouter) SubRoute(path string) *SubRouter {
	if !strings.HasPrefix(path, "/") {
		panic("path must begin with '/' in path '" + path + "'")
	}

	if strings.HasSuffix(path, "/") {
		path = strings.TrimSuffix(path, "/")
	}

	newPath := s.path + path

	return &SubRouter{
		path:                newPath,
		router:              s.router,
		perRouteMiddlewares: s.perRouteMiddlewares,
		middlewares:         s.middlewares,
		log:                 s.log,
	}
}

func (s *SubRouter) Route(method, path string, endpoint HandlerFunc) {
	handler := wrapHandler(endpoint, s.log)

	s.Handler(method, path, handler)
}

func (s *SubRouter) Handler(method, path string, handler http.Handler) {
	if !strings.HasPrefix(path, "/") {
		panic("path must begin with '/' in path '" + path + "'")
	}

	fullPath := s.path + path

	handler = wrapMiddleware(handler, s.middlewares...)
	handler = wrapPerRouteMiddleware(fullPath, handler, s.perRouteMiddlewares...)

	s.router.Handler(method, fullPath, handler)
}

func (s *SubRouter) RouteFiles(path string, root http.FileSystem) {
	const filePathSuffix = "/*filepath"

	if !strings.HasPrefix(path, "/") {
		panic("path must begin with '/' in path '" + path + "'")
	}

	if strings.HasSuffix(path, "/") {
		path = strings.TrimSuffix(path, "/")
	}

	if !strings.HasSuffix(path, filePathSuffix) {
		path += filePathSuffix
	}

	fileServer := http.FileServer(root)
	fileServer = wrapMiddleware(fileServer, s.middlewares...)
	fileServer = wrapPerRouteMiddleware(path, fileServer, s.perRouteMiddlewares...)

	s.router.GET(path, func(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		req.URL.Path = ps.ByName("filepath")
		fileServer.ServeHTTP(w, req)
	})
}

func (s *SubRouter) Use(m ...Middleware) {
	s.middlewares = append(s.middlewares, m...)
}

func (s *SubRouter) UsePerRoute(m ...PerRouteMiddleware) {
	s.perRouteMiddlewares = append(s.perRouteMiddlewares, m...)
}
